package store

import (
	"sync"

	"hlc/internal/dict"
	"hlc/internal/model"
)

const (
	batchSize = 10000
)

var (
	timestamp    int32
	storageSize  int
	isFirstPhase = true
	wordbook     = dict.New()
	updater      = make(chan *model.DBItem, 1000)

	database    sync.Mutex
	batchAccess []sync.RWMutex
	accounts    [][batchSize]model.DBItem

	emptyList = make([]map[string]interface{}, 0, 0)
)

func likesMatcher(in []model.LikesItem) []int32 {
	list := map[int32]struct{}{}
	for _, like := range in {
		list[like.ID] = struct{}{}
	}
	out := make([]int32, 0, len(list))
	for k := range list {
		out = append(out, k)
	}
	return out
}

func interestMatcher(in []string) map[uint8]struct{} {
	out := make(map[uint8]struct{}, len(in))
	for _, i := range in {
		out[wordbook.Interests.Set(i)] = struct{}{}
	}
	return out
}

// Suggest -
func Suggest(id int32, limit int) ([]map[string]interface{}, error) {
	if err := isAccExists(id); err != nil {
		return nil, err
	}
	return emptyList, nil
}

// AccInfo -
func AccInfo(id int32) *model.DBItem {
	acc := model.NewInt24(id)
	return &accounts[acc.Shift][acc.Value]
}
