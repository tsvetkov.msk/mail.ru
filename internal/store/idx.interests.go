package store

import (
	"context"
	"hlc/internal/model"
	"net/url"
	"sort"
	"strings"
	"sync"
)

type idxInterests struct {
	access sync.Mutex
	dirty  map[uint8]bool
	store  map[uint8][]*model.DBItem
	stat   map[uint8]map[string]groupCounter
}

func (idx *idxInterests) Upload() {
	for i, batch := range accounts {
		for j, acc := range batch {
			for interes := range acc.Interests {
				if idx.store[interes] == nil {
					idx.createBatch(interes)
				}
				idx.dirty[interes] = true
				idx.store[interes] = append(idx.store[interes], &accounts[i][j])

				idx.stat[interes]["sex"].Incr(int(acc.Sex))
				idx.stat[interes]["status"].Incr(int(acc.Status))
				idx.stat[interes]["city"].Incr(int(acc.City))
				idx.stat[interes]["country"].Incr(int(acc.Country))
				idx.stat[interes]["country,status"].Incr(int(acc.Country), int(acc.Status))
				idx.stat[interes]["country,sex"].Incr(int(acc.Country), int(acc.Sex))
			}
		}
	}
}

func (idx *idxInterests) createBatch(interes uint8) {
	idx.store[interes] = make([]*model.DBItem, 0)
	idx.stat[interes] = map[string]groupCounter{
		"sex":            &groupSexCounter{store: make(map[uint8]int)},
		"status":         &groupStatusCounter{store: make(map[uint8]int)},
		"city":           &groupCityCounter{store: make(map[uint16]int)},
		"country":        &groupCountryCounter{store: make(map[uint8]int)},
		"country,status": &groupCountryStatusCounter{store: make(map[uint8]map[uint8]int)},
		"country,sex":    &groupCountrySexCounter{store: make(map[uint8]map[uint8]int)},
	}
}

func (idx *idxInterests) Update(acc *model.DBItem, from, to map[uint8]struct{}) {

	idx.access.Lock()
	defer idx.access.Unlock()

	for interes := range from {
		for i, user := range idx.store[interes] {
			if user.ID == acc.ID {
				idx.store[interes] = append(idx.store[interes][:i], idx.store[interes][i+1:]...)
				break
			}
		}
	}

	for interes := range to {
		if idx.store[interes] == nil {
			idx.createBatch(interes)
		}
		idx.dirty[interes] = true
		idx.store[interes] = append(idx.store[interes], acc)
	}
}

func (idx *idxInterests) Insert(acc *model.DBItem) {

	idx.access.Lock()
	defer idx.access.Unlock()

	for interes := range acc.Interests {
		if idx.store[interes] == nil {
			idx.createBatch(interes)
		}
		idx.dirty[interes] = true
		idx.store[interes] = append(idx.store[interes], acc)
		idx.stat[interes]["sex"].Incr(int(acc.Sex))
		idx.stat[interes]["status"].Incr(int(acc.Status))
		idx.stat[interes]["city"].Incr(int(acc.City))
		idx.stat[interes]["country"].Incr(int(acc.Country))
		idx.stat[interes]["country,status"].Incr(int(acc.Country), int(acc.Status))
		idx.stat[interes]["country,sex"].Incr(int(acc.Country), int(acc.Sex))
	}
}

func (idx *idxInterests) Construct() {

	idx.access.Lock()
	defer idx.access.Unlock()

	for interes := range idx.store {
		if idx.dirty[interes] {
			sort.Slice(idx.store[interes], func(i, j int) bool {
				return idx.store[interes][i].ID > idx.store[interes][j].ID
			})
		}
		idx.dirty[interes] = false
	}
}

func (idx *idxInterests) Filter(ctx context.Context, query url.Values, limit int) ([]map[string]interface{}, error) {

	ids := []uint8{}
	for _, sid := range strings.Split(query.Get("interests_contains"), ",") {
		id := wordbook.Interests.Get(sid)
		if id == 0 {
			return emptyList, nil
		}
		ids = append(ids, id)
	}

	target := 0
	count := batchSize * storageSize
	filters := []filterFn{}

	if len(ids) > 1 {
		for i, id := range ids {
			if len(idx.store[id]) < count {
				target = i
			}
		}
		filters = append(filters, interestsContains(append(ids[:target], ids[target+1:]...)))
	}

	// формируем фильтры
	if fls, ok := createFilters(query, "interests_contains"); ok {
		filters = append(filters, fls...)
	} else {
		// один из фильтров всегда false
		return emptyList, nil
	}

	accs := make([]map[string]interface{}, 0, limit)
	for _, acc := range idx.store[ids[target]] {
		select {
		case <-ctx.Done():
			return accs, ctx.Err()
		default:
		}

		if ok := applyFilters(acc, filters); !ok {
			continue
		}

		accs = append(accs, getResponseItem(acc, query))
		if len(accs) == limit {
			break
		}
	}

	return accs, nil
}

func (idx *idxInterests) Scan(query url.Values) <-chan *model.DBItem {
	ch := make(chan *model.DBItem, 1)
	go func() {

		defer close(ch)

		id := wordbook.Interests.Get(query.Get("interests"))
		if id == 0 {
			return
		}

		filters := []filterFn{}

		if birth := query.Get("birth"); birth != "" {
			fn, ok := availableFilters["birth_year"](birth)
			if !ok {
				return
			}
			filters = append(filters, fn)
		}
		if joined := query.Get("joined"); joined != "" {
			fn, ok := availableFilters["joined_year"](joined)
			if !ok {
				return
			}
			filters = append(filters, fn)
		}

		for _, acc := range idx.store[id] {
			if len(filters) > 0 {
				if ok := applyFilters(acc, filters); !ok {
					continue
				}
			}
			ch <- acc
		}
	}()
	return ch
}

func interestsContains(interests []uint8) filterFn {
	return func(acc *model.DBItem) bool {
	main:
		for _, interes := range interests {
			for id := range acc.Interests {
				if id == interes {
					continue main
				}
			}
			return false
		}
		return true
	}
}

func (idx *idxInterests) Stat(query url.Values, key string) (groupCounter, bool) {

	//fmt.Println(query, key)

	if len(query) == 1 {
		if interest := wordbook.Interests.Get(query.Get("interests")); interest > 0 {
			if groups, ok := idx.stat[interest][key]; ok {
				return groups, true
			}
		}

	}
	return nil, false
}
