package store

import (
	"context"
	"hlc/internal/model"
	"hlc/internal/utils"
	"net/url"
	"sort"
	"sync"
)

type idxCountry struct {
	access sync.RWMutex
	dirty  map[uint8]map[uint8]bool
	store  map[uint8]map[uint8][]*model.DBItem
	stat   map[uint8]map[string]groupCounter
}

func (idx *idxCountry) Upload() {
	for i, batch := range accounts {
		for j, acc := range batch {
			country := acc.Country
			// if country := acc.Country; country > 0 {
			sexStatus := acc.Sex | acc.Status
			if idx.store[country] == nil {
				idx.store[country] = make(map[uint8][]*model.DBItem, 0)
				idx.dirty[country] = make(map[uint8]bool)
				idx.stat[country] = map[string]groupCounter{
					"sex":       &groupSexCounter{store: make(map[uint8]int)},
					"status":    &groupStatusCounter{store: make(map[uint8]int)},
					"interests": &groupInterestsCounter{store: make(map[uint8]int)},
				}
			}
			if idx.store[country][sexStatus] == nil {
				idx.store[country][sexStatus] = make([]*model.DBItem, 0)
				idx.dirty[country][sexStatus] = true
			}
			idx.store[country][sexStatus] = append(idx.store[country][sexStatus], &accounts[i][j])
			idx.stat[country]["sex"].Incr(int(acc.Sex))
			idx.stat[country]["status"].Incr(int(acc.Status))
			for k := range acc.Interests {
				idx.stat[acc.Country]["interests"].Incr(int(k))
			}
			// }
		}
	}
}

func (idx *idxCountry) Insert(acc *model.DBItem) {

	if acc.Country == 0 {
		// return
	}

	idx.access.Lock()
	defer idx.access.Unlock()

	sexStatus := acc.Sex | acc.Status
	if idx.store[acc.Country] == nil {
		idx.store[acc.Country] = make(map[uint8][]*model.DBItem, 0)
		idx.dirty[acc.Country] = make(map[uint8]bool)
		idx.stat[acc.Country] = map[string]groupCounter{
			"sex":       &groupSexCounter{store: make(map[uint8]int)},
			"status":    &groupStatusCounter{store: make(map[uint8]int)},
			"interests": &groupInterestsCounter{store: make(map[uint8]int)},
		}
	}
	if idx.store[acc.Country][sexStatus] == nil {
		idx.store[acc.Country][sexStatus] = make([]*model.DBItem, 0)
		idx.dirty[acc.Country][sexStatus] = true
	}
	idx.dirty[acc.Country][sexStatus] = true
	idx.store[acc.Country][sexStatus] = append(idx.store[acc.Country][sexStatus], acc)
	idx.stat[acc.Country]["sex"].Incr(int(acc.Sex))
	idx.stat[acc.Country]["status"].Incr(int(acc.Status))
	for k := range acc.Interests {
		idx.stat[acc.Country]["interests"].Incr(int(k))
	}
}

func (idx *idxCountry) Update(acc *model.DBItem, country uint8, sex, status uint8) {

	idx.access.Lock()
	defer idx.access.Unlock()

	// if acc.Country > 0 {
	sexStatus := acc.Sex | acc.Status
	for i, user := range idx.store[acc.Country][sexStatus] {
		if user.ID == acc.ID {
			idx.store[acc.Country][sexStatus] = append(idx.store[acc.Country][sexStatus][:i], idx.store[acc.Country][sexStatus][i+1:]...)
			break
		}
	}
	idx.stat[acc.Country]["sex"].Decr(int(acc.Sex))
	idx.stat[acc.Country]["status"].Decr(int(acc.Status))

	// }
	// if country > 0 {
	sexStatus = sex | status
	if idx.store[country] == nil {
		idx.store[country] = make(map[uint8][]*model.DBItem, 0)
		idx.dirty[country] = make(map[uint8]bool)
		idx.stat[country] = map[string]groupCounter{
			"sex":       &groupSexCounter{store: make(map[uint8]int)},
			"status":    &groupStatusCounter{store: make(map[uint8]int)},
			"interests": &groupInterestsCounter{store: make(map[uint8]int)},
		}
	}
	if idx.store[country][sexStatus] == nil {
		idx.store[country][sexStatus] = make([]*model.DBItem, 0)
		idx.dirty[country][sexStatus] = true
	}
	idx.dirty[country][sexStatus] = true
	idx.store[country][sexStatus] = append(idx.store[country][sexStatus], acc)
	idx.stat[country]["sex"].Incr(int(sex))
	idx.stat[country]["status"].Incr(int(status))
	// }
}

func (idx *idxCountry) Construct() {

	idx.access.Lock()
	defer idx.access.Unlock()

	for country := range idx.store {
		for sest := range idx.store[country] {
			if idx.dirty[country][sest] {
				sort.Slice(idx.store[country][sest], func(i, j int) bool {
					return idx.store[country][sest][i].ID > idx.store[country][sest][j].ID
				})
			}
			idx.dirty[country][sest] = false
		}
	}
}

func (idx *idxCountry) Null(ctx context.Context, query url.Values, limit int) ([]map[string]interface{}, error) {

	filters, ok := createFilters(query, "sex_eq", "status_eq", "status_neq", "country_null")
	if !ok {
		// один из фильтров всегда false
		return emptyList, nil
	}

	country := uint8(0)

	sex := wordbook.Sex.Get(query.Get("sex_eq"))
	equal := true
	status := wordbook.Status.Get(query.Get("status_eq"))
	if status == 0 {
		status = wordbook.Status.Get(query.Get("status_neq"))
		equal = false
	}

	batches := [][]*model.DBItem{}
	for sest := range idx.store[country] {
		if len(idx.store[country][sest]) == 0 {
			continue
		}
		if sex == 0 || sex&sest > 0 {
			if status == 0 {
				batches = append(batches, idx.store[country][sest])
				continue
			}
			if (equal && status&sest > 0) || (!equal && status&sest == 0) {
				batches = append(batches, idx.store[country][sest])
			}
		}
	}

	done := make(chan bool, 0)
	defer close(done)

	accs := make([]map[string]interface{}, 0, limit)

	for acc := range utils.MergeHeap(done, batches...) {
		select {
		case <-ctx.Done():
			return accs, ctx.Err()
		default:
		}

		if ok := applyFilters(acc, filters); !ok {
			continue
		}

		accs = append(accs, getResponseItem(acc, query))
		if len(accs) == limit {
			break
		}
	}

	return accs, nil
}

func (idx *idxCountry) Filter(ctx context.Context, query url.Values, limit int) ([]map[string]interface{}, error) {

	filters, ok := createFilters(query, "sex_eq", "status_eq", "status_neq", "country_eq")
	if !ok {
		// один из фильтров всегда false
		return emptyList, nil
	}

	country := wordbook.Country.Get(query.Get("country_eq"))
	if country == 0 || idx.store[country] == nil {
		return emptyList, nil
	}

	sex := wordbook.Sex.Get(query.Get("sex_eq"))
	equal := true
	status := wordbook.Status.Get(query.Get("status_eq"))
	if status == 0 {
		status = wordbook.Status.Get(query.Get("status_neq"))
		equal = false
	}

	batches := [][]*model.DBItem{}
	for sest := range idx.store[country] {
		if len(idx.store[country][sest]) == 0 {
			continue
		}
		if sex == 0 || sex&sest > 0 {
			if status == 0 {
				batches = append(batches, idx.store[country][sest])
				continue
			}
			if (equal && status&sest > 0) || (!equal && status&sest == 0) {
				batches = append(batches, idx.store[country][sest])
			}
		}
	}

	done := make(chan bool, 0)
	defer close(done)

	accs := make([]map[string]interface{}, 0, limit)

	for acc := range utils.MergeHeap(done, batches...) {
		select {
		case <-ctx.Done():
			return accs, ctx.Err()
		default:
		}

		if ok := applyFilters(acc, filters); !ok {
			continue
		}

		accs = append(accs, getResponseItem(acc, query))
		if len(accs) == limit {
			break
		}
	}

	return accs, nil
}

func (idx *idxCountry) Stat(query url.Values, key string) (groupCounter, bool) {
	if len(query) > 1 {
		return nil, false
	}
	id := wordbook.Country.Get(query.Get("country"))
	if id == 0 {
		return nil, false
	}

	if idx.stat[id] == nil {
		return nil, false
	}

	groups, ok := idx.stat[id][key]
	if ok {
		return groups, true
	}
	return nil, false
}

func (idx *idxCountry) Scan(query url.Values) <-chan *model.DBItem {
	ch := make(chan *model.DBItem, 1)
	go func() {

		defer close(ch)

		id := wordbook.Country.Get(query.Get("country"))
		if id == 0 {
			return
		}

		filters := []filterFn{}

		if birth := query.Get("birth"); birth != "" {
			fn, ok := availableFilters["birth_year"](birth)
			if !ok {
				return
			}
			filters = append(filters, fn)
		}
		if joined := query.Get("joined"); joined != "" {
			fn, ok := availableFilters["joined_year"](joined)
			if !ok {
				return
			}
			filters = append(filters, fn)
		}

		batches := [][]*model.DBItem{}
		for _, batch := range idx.store[id] {
			if len(batch) == 0 {
				continue
			}
			batches = append(batches, batch)
		}

		done := make(chan bool, 0)
		defer close(done)

		for acc := range utils.MergeHeap(done, batches...) {
			if len(filters) > 0 {
				if ok := applyFilters(acc, filters); !ok {
					continue
				}
			}
			ch <- acc
		}
	}()
	return ch
}
