package store

var (
	sexStatusCursor    *idxSexStatus
	likeContainsCursor *idxLikeContains
	cityCursor         *idxCity
	countryCursor      *idxCountry
	interestsCursor    *idxInterests
	birthCursor        *idxBirth
	phoneCursor        *idxPhone
	fnameCursor        *idxFname
	snameCursor        *idxSname
	joinedCursor       *idxJoined
)
