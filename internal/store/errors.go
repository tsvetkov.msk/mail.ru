package store

import "fmt"

var (

	// ErrUserNotFound -
	ErrUserNotFound = fmt.Errorf("User not found")

	// ErrEmailExists -
	ErrEmailExists = fmt.Errorf("Email exists")

	// ErrPhoneExists -
	ErrPhoneExists = fmt.Errorf("Phone exists")

	// ErrIncorrectValue -
	ErrIncorrectValue = fmt.Errorf("Incorrect value")
)
