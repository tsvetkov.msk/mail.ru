package store

import (
	"context"
	"hlc/internal/model"
	"net/url"
	"sort"
	"sync"
	"time"
)

type idxBirth struct {
	access sync.Mutex
	dirty  map[string]bool
	store  map[string][]*model.DBItem
	stat   map[string]map[string]groupCounter
	query  map[string]map[string]map[string]map[uint8]groupCounter
}

func (idx *idxBirth) addBatch(year string) {
	if idx.store[year] == nil {
		idx.store[year] = make([]*model.DBItem, 0)
		idx.stat[year] = map[string]groupCounter{
			"sex":       &groupSexCounter{store: make(map[uint8]int)},
			"status":    &groupStatusCounter{store: make(map[uint8]int)},
			"interests": &groupInterestsCounter{store: make(map[uint8]int)},
			"country":   &groupCountryCounter{store: make(map[uint8]int)},
			"city":      &groupCityCounter{store: make(map[uint16]int)},
			"city,status": &groupCityStatusCounter{
				store: make(map[uint16]map[uint8]int),
			},
			"country,status": &groupCountryStatusCounter{
				store: make(map[uint8]map[uint8]int),
			},
			"city,sex": &groupCitySexCounter{
				store: make(map[uint16]map[uint8]int),
			},
		}
		idx.query[year] = map[string]map[string]map[uint8]groupCounter{
			"sex": map[string]map[uint8]groupCounter{
				"city,status": map[uint8]groupCounter{
					1: &groupCityStatusCounter{
						store: make(map[uint16]map[uint8]int),
					},
					2: &groupCityStatusCounter{
						store: make(map[uint16]map[uint8]int),
					},
				},
				"country,status": map[uint8]groupCounter{
					1: &groupCountryStatusCounter{
						store: make(map[uint8]map[uint8]int),
					},
					2: &groupCountryStatusCounter{
						store: make(map[uint8]map[uint8]int),
					},
				},
				"country,sex": map[uint8]groupCounter{
					1: &groupCountrySexCounter{
						store: make(map[uint8]map[uint8]int),
					},
					2: &groupCountrySexCounter{
						store: make(map[uint8]map[uint8]int),
					},
				},
				"city,sex": map[uint8]groupCounter{
					1: &groupCitySexCounter{
						store: make(map[uint16]map[uint8]int),
					},
					2: &groupCitySexCounter{
						store: make(map[uint16]map[uint8]int),
					},
				},
				"country": map[uint8]groupCounter{
					1: &groupCountryCounter{
						store: make(map[uint8]int),
					},
					2: &groupCountryCounter{
						store: make(map[uint8]int),
					},
				},
				"city": map[uint8]groupCounter{
					1: &groupCityCounter{
						store: make(map[uint16]int),
					},
					2: &groupCityCounter{
						store: make(map[uint16]int),
					},
				},
			},
			"status": map[string]map[uint8]groupCounter{
				"city,status": map[uint8]groupCounter{
					4: &groupCityStatusCounter{
						store: make(map[uint16]map[uint8]int),
					},
					8: &groupCityStatusCounter{
						store: make(map[uint16]map[uint8]int),
					},
					16: &groupCityStatusCounter{
						store: make(map[uint16]map[uint8]int),
					},
				},
				"city,sex": map[uint8]groupCounter{
					4: &groupCitySexCounter{
						store: make(map[uint16]map[uint8]int),
					},
					8: &groupCitySexCounter{
						store: make(map[uint16]map[uint8]int),
					},
					16: &groupCitySexCounter{
						store: make(map[uint16]map[uint8]int),
					},
				},
				"country,status": map[uint8]groupCounter{
					4: &groupCountryStatusCounter{
						store: make(map[uint8]map[uint8]int),
					},
					8: &groupCountryStatusCounter{
						store: make(map[uint8]map[uint8]int),
					},
					16: &groupCountryStatusCounter{
						store: make(map[uint8]map[uint8]int),
					},
				},
				"country,sex": map[uint8]groupCounter{
					4: &groupCountrySexCounter{
						store: make(map[uint8]map[uint8]int),
					},
					8: &groupCountrySexCounter{
						store: make(map[uint8]map[uint8]int),
					},
					16: &groupCountrySexCounter{
						store: make(map[uint8]map[uint8]int),
					},
				},
				"country": map[uint8]groupCounter{
					4: &groupCountryCounter{
						store: make(map[uint8]int),
					},
					8: &groupCountryCounter{
						store: make(map[uint8]int),
					},
					16: &groupCountryCounter{
						store: make(map[uint8]int),
					},
				},
				"city": map[uint8]groupCounter{
					4: &groupCityCounter{
						store: make(map[uint16]int),
					},
					8: &groupCityCounter{
						store: make(map[uint16]int),
					},
					16: &groupCityCounter{
						store: make(map[uint16]int),
					},
				},
			},
		}
	}
}

func (idx *idxBirth) Upload() {
	for i, batch := range accounts {
		for j, acc := range batch {
			year := time.Unix(int64(acc.Birth), 0).Format("2006")
			if idx.store[year] == nil {
				idx.addBatch(year)
			}
			idx.dirty[year] = true
			idx.store[year] = append(idx.store[year], &accounts[i][j])

			idx.stat[year]["sex"].Incr(int(acc.Sex))
			idx.stat[year]["status"].Incr(int(acc.Status))
			idx.stat[year]["city"].Incr(int(acc.City))
			idx.stat[year]["country"].Incr(int(acc.Country))
			for k := range acc.Interests {
				idx.stat[year]["interests"].Incr(int(k))
			}
			idx.stat[year]["city,status"].Incr(int(acc.City), int(acc.Status))
			idx.stat[year]["country,status"].Incr(int(acc.Country), int(acc.Status))
			idx.stat[year]["city,sex"].Incr(int(acc.City), int(acc.Sex))

			idx.query[year]["sex"]["city,status"][acc.Sex].Incr(int(acc.City), int(acc.Status))
			idx.query[year]["status"]["city,status"][acc.Status].Incr(int(acc.City), int(acc.Status))

			idx.query[year]["status"]["city,sex"][acc.Status].Incr(int(acc.City), int(acc.Sex))
			idx.query[year]["sex"]["city,sex"][acc.Sex].Incr(int(acc.City), int(acc.Sex))

			idx.query[year]["sex"]["country,sex"][acc.Sex].Incr(int(acc.Country), int(acc.Sex))
			idx.query[year]["status"]["country,sex"][acc.Status].Incr(int(acc.Country), int(acc.Sex))

			idx.query[year]["sex"]["country,status"][acc.Sex].Incr(int(acc.Country), int(acc.Status))
			idx.query[year]["status"]["country,status"][acc.Status].Incr(int(acc.Country), int(acc.Status))

			idx.query[year]["sex"]["country"][acc.Sex].Incr(int(acc.Country))
			idx.query[year]["status"]["country"][acc.Status].Incr(int(acc.Country))

			idx.query[year]["sex"]["city"][acc.Sex].Incr(int(acc.City))
			idx.query[year]["status"]["city"][acc.Status].Incr(int(acc.City))
		}
	}
}

func (idx *idxBirth) Insert(acc *model.DBItem) {

	idx.access.Lock()
	defer idx.access.Unlock()

	year := time.Unix(int64(acc.Birth), 0).Format("2006")

	if idx.store[year] == nil {
		idx.addBatch(year)
	}
	idx.dirty[year] = true
	idx.store[year] = append(idx.store[year], acc)

	idx.stat[year]["sex"].Incr(int(acc.Sex))
	idx.stat[year]["status"].Incr(int(acc.Status))
	idx.stat[year]["country"].Incr(int(acc.Country))
	idx.stat[year]["city"].Incr(int(acc.City))
	for k := range acc.Interests {
		idx.stat[year]["interests"].Incr(int(k))
	}
	idx.stat[year]["city,status"].Incr(int(acc.City), int(acc.Status))
	idx.stat[year]["country,status"].Incr(int(acc.Country), int(acc.Status))
	idx.stat[year]["city,sex"].Incr(int(acc.City), int(acc.Sex))

	idx.query[year]["sex"]["city,status"][acc.Sex].Incr(int(acc.City), int(acc.Status))
	idx.query[year]["status"]["city,status"][acc.Status].Incr(int(acc.City), int(acc.Status))

	idx.query[year]["status"]["city,sex"][acc.Status].Incr(int(acc.City), int(acc.Sex))
	idx.query[year]["sex"]["city,sex"][acc.Sex].Incr(int(acc.City), int(acc.Sex))

	idx.query[year]["sex"]["country,sex"][acc.Sex].Incr(int(acc.Country), int(acc.Sex))
	idx.query[year]["status"]["country,sex"][acc.Status].Incr(int(acc.Country), int(acc.Sex))

	idx.query[year]["sex"]["country,status"][acc.Sex].Incr(int(acc.Country), int(acc.Status))
	idx.query[year]["status"]["country,status"][acc.Status].Incr(int(acc.Country), int(acc.Status))

	idx.query[year]["sex"]["country"][acc.Sex].Incr(int(acc.Country))
	idx.query[year]["status"]["country"][acc.Status].Incr(int(acc.Country))

	idx.query[year]["sex"]["city"][acc.Sex].Incr(int(acc.City))
	idx.query[year]["status"]["city"][acc.Status].Incr(int(acc.City))
}

func (idx *idxBirth) Update(acc *model.DBItem, from int32, to int32, sex uint8, status uint8) {

	if from == to {
		return
	}

	idx.access.Lock()
	defer idx.access.Unlock()

	yearFrom := time.Unix(int64(from), 0).Format("2006")
	for i, user := range idx.store[yearFrom] {
		if user.ID == acc.ID {
			idx.store[yearFrom] = append(idx.store[yearFrom][:i], idx.store[yearFrom][i+1:]...)
			break
		}
		idx.stat[yearFrom]["sex"].Decr(int(acc.Sex))
		idx.stat[yearFrom]["status"].Decr(int(acc.Status))
	}

	yearTo := time.Unix(int64(to), 0).Format("2006")
	if idx.store[yearTo] == nil {
		idx.addBatch(yearTo)
	}
	idx.dirty[yearTo] = true
	idx.store[yearTo] = append(idx.store[yearTo], acc)
	idx.stat[yearTo]["sex"].Incr(int(acc.Sex))
	idx.stat[yearTo]["status"].Incr(int(acc.Status))
	idx.stat[yearTo]["city"].Incr(int(acc.City))
	idx.stat[yearTo]["country"].Incr(int(acc.Country))
	for k := range acc.Interests {
		idx.stat[yearTo]["interests"].Incr(int(k))
	}
	idx.stat[yearTo]["city,status"].Incr(int(acc.City), int(acc.Status))
	idx.stat[yearTo]["country,status"].Incr(int(acc.Country), int(acc.Status))
	idx.stat[yearTo]["city,sex"].Incr(int(acc.City), int(acc.Sex))

	idx.query[yearTo]["sex"]["city,status"][acc.Sex].Incr(int(acc.City), int(acc.Status))
	idx.query[yearTo]["status"]["city,status"][acc.Status].Incr(int(acc.City), int(acc.Status))

	idx.query[yearTo]["status"]["city,sex"][acc.Status].Incr(int(acc.City), int(acc.Sex))
	idx.query[yearTo]["sex"]["city,sex"][acc.Sex].Incr(int(acc.City), int(acc.Sex))

	idx.query[yearTo]["sex"]["country,sex"][acc.Sex].Incr(int(acc.Country), int(acc.Sex))
	idx.query[yearTo]["status"]["country,sex"][acc.Status].Incr(int(acc.Country), int(acc.Sex))

	idx.query[yearTo]["sex"]["country,status"][acc.Sex].Incr(int(acc.Country), int(acc.Status))
	idx.query[yearTo]["status"]["country,status"][acc.Status].Incr(int(acc.Country), int(acc.Status))

	idx.query[yearTo]["sex"]["country"][acc.Sex].Incr(int(acc.Country))
	idx.query[yearTo]["status"]["country"][acc.Status].Incr(int(acc.Country))

	idx.query[yearTo]["sex"]["city"][acc.Sex].Incr(int(acc.City))
	idx.query[yearTo]["status"]["city"][acc.Status].Incr(int(acc.City))
}

func (idx *idxBirth) Construct() {

	idx.access.Lock()
	defer idx.access.Unlock()

	for year := range idx.store {
		if idx.dirty[year] {
			sort.Slice(idx.store[year], func(i, j int) bool {
				return idx.store[year][i].ID > idx.store[year][j].ID
			})
		}
		idx.dirty[year] = false
	}
}

func (idx *idxBirth) Stat(query url.Values, key string) (groupCounter, bool) {
	year := query.Get("birth")
	if idx.stat[year] == nil {
		return nil, false
	}

	if len(query) == 1 {
		if groups, ok := idx.stat[year][key]; ok {
			return groups, true
		}
	}
	if len(query) == 2 {

		if sex := query.Get("sex"); sex != "" {
			if batch, ok := idx.query[year]["sex"]; ok {
				if page, ok := batch[key]; ok {
					if groups, ok := page[wordbook.Sex.Get(sex)]; ok {
						return groups, true
					}
				}
			}
		}
		if status := query.Get("status"); status != "" {
			if batch, ok := idx.query[year]["status"]; ok {
				if page, ok := batch[key]; ok {
					if groups, ok := page[wordbook.Status.Get(status)]; ok {
						return groups, true
					}
				}
			}
		}
	}
	return nil, false
}

func (idx *idxBirth) Filter(ctx context.Context, query url.Values, limit int) ([]map[string]interface{}, error) {

	filters, ok := createFilters(query, "birth_year")
	if !ok {
		// один из фильтров всегда false
		return emptyList, nil
	}

	accs := make([]map[string]interface{}, 0, limit)

	for _, acc := range idx.store[query.Get("birth_year")] {
		select {
		case <-ctx.Done():
			return accs, ctx.Err()
		default:
		}

		if ok := applyFilters(acc, filters); !ok {
			continue
		}

		accs = append(accs, getResponseItem(acc, query))
		if len(accs) == limit {
			break
		}
	}

	return accs, nil
}

func (idx *idxBirth) Scan(query url.Values) <-chan *model.DBItem {
	ch := make(chan *model.DBItem, 1)
	go func() {

		defer close(ch)

		filters := []filterFn{}
		if sex := query.Get("sex"); sex != "" {
			fn, ok := availableFilters["sex_eq"](sex)
			if !ok {
				return
			}
			filters = append(filters, fn)
		}
		if status := query.Get("status"); status != "" {
			fn, ok := availableFilters["status_eq"](status)
			if !ok {
				return
			}
			filters = append(filters, fn)
		}

		for _, acc := range idx.store[query.Get("birth")] {
			if len(filters) > 0 {
				if ok := applyFilters(acc, filters); !ok {
					continue
				}
			}
			ch <- acc
		}
	}()
	return ch
}
