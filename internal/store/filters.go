package store

import (
	"hlc/internal/model"
	"hlc/internal/utils"
	"net/url"
	"strconv"
	"strings"
	"time"
)

type filterFn func(*model.DBItem) bool

func createFilters(query url.Values, exclude ...string) ([]filterFn, bool) {
	filters := []filterFn{}
	for _, f := range utils.MatchURL(query, exclude...) {
		filterCreator, ok := availableFilters[f]
		if !ok {
			return nil, false
		}
		filter, ok := filterCreator(query.Get(f))
		if !ok {
			return nil, false
		}
		filters = append(filters, filter)
	}
	return filters, true
}

func applyFilters(acc *model.DBItem, filters []filterFn) bool {
	for _, fn := range filters {
		if ok := fn(acc); !ok {
			return false
		}
	}
	return true
}

var (
	availableFilters = map[string]func(string) (filterFn, bool){
		"sex_eq": func(v string) (filterFn, bool) {
			sex := wordbook.Sex.Get(v)
			if sex == 0 {
				return nil, false
			}
			return func(acc *model.DBItem) bool {
				return acc.Sex == sex
			}, true
		},
		"email_domain": func(v string) (filterFn, bool) {
			domain := wordbook.Email.Get(v)
			if domain == 0 {
				return nil, false
			}
			return func(acc *model.DBItem) bool {
				return acc.Email.Domain == domain
			}, true
		},
		"email_gt": func(v string) (filterFn, bool) {
			return func(acc *model.DBItem) bool {
				return acc.Email.Name > v
			}, true
		},
		"email_lt": func(v string) (filterFn, bool) {
			return func(acc *model.DBItem) bool {
				return acc.Email.Name < v
			}, true
		},
		"status_eq": func(v string) (filterFn, bool) {
			status := wordbook.Status.Get(v)
			if status == 0 {
				return nil, false
			}
			return func(acc *model.DBItem) bool {
				return acc.Status == status
			}, true
		},
		"status_neq": func(v string) (filterFn, bool) {
			status := wordbook.Status.Get(v)
			if status == 0 {
				return nil, false
			}
			return func(acc *model.DBItem) bool {
				return acc.Status != status
			}, true
		},
		"fname_eq": func(v string) (filterFn, bool) {
			name := wordbook.FName.Get(v)
			if name == 0 {
				return nil, false
			}
			return func(acc *model.DBItem) bool {
				return acc.Fname == name
			}, true
		},
		"fname_any": func(v string) (filterFn, bool) {
			fnames := map[uint8]struct{}{}
			for _, n := range strings.Split(v, ",") {
				nid := wordbook.FName.Get(n)
				if nid == 0 {
					continue
				}
				fnames[nid] = struct{}{}
			}
			return func(acc *model.DBItem) bool {
				if _, ok := fnames[acc.Fname]; ok {
					return true
				}
				return false
			}, true
		},
		"fname_null_1": func(v string) (filterFn, bool) {
			return func(acc *model.DBItem) bool {
				return acc.Fname == 0
			}, true
		},
		"fname_null_0": func(v string) (filterFn, bool) {
			return func(acc *model.DBItem) bool {
				return acc.Fname != 0
			}, true
		},
		"sname_eq": func(v string) (filterFn, bool) {
			name := wordbook.SName.Get(v)
			if name == 0 {
				return nil, false
			}
			return func(acc *model.DBItem) bool {
				return acc.Sname == name
			}, true
		},
		"sname_starts": func(v string) (filterFn, bool) {
			return func(acc *model.DBItem) bool {
				return strings.HasPrefix(wordbook.SName.Conv(acc.Sname), v)
			}, true
		},
		"sname_null_1": func(v string) (filterFn, bool) {
			return func(acc *model.DBItem) bool {
				return acc.Sname == 0
			}, true
		},
		"sname_null_0": func(v string) (filterFn, bool) {
			return func(acc *model.DBItem) bool {
				return acc.Sname != 0
			}, true
		},
		"phone_code": func(v string) (filterFn, bool) {
			code := "(" + v + ")"
			return func(acc *model.DBItem) bool {
				if acc.Phone == "" {
					return false
				}
				return strings.Index(acc.Phone, code) != -1
			}, true
		},
		"phone_null_1": func(v string) (filterFn, bool) {
			return func(acc *model.DBItem) bool {
				return acc.Phone == ""
			}, true
		},
		"phone_null_0": func(v string) (filterFn, bool) {
			return func(acc *model.DBItem) bool {
				return acc.Phone != ""
			}, true
		},
		"country_eq": func(v string) (filterFn, bool) {
			name := wordbook.Country.Get(v)
			if name == 0 {
				return nil, false
			}
			return func(acc *model.DBItem) bool {
				return acc.Country == name
			}, true
		},
		"country_null_1": func(v string) (filterFn, bool) {
			return func(acc *model.DBItem) bool {
				return acc.Country == 0
			}, true
		},
		"country_null_0": func(v string) (filterFn, bool) {
			return func(acc *model.DBItem) bool {
				return acc.Country > 0
			}, true
		},
		"city_eq": func(v string) (filterFn, bool) {
			name := wordbook.City.Get(v)
			if name == 0 {
				return nil, false
			}
			return func(acc *model.DBItem) bool {
				return acc.City == name
			}, true
		},
		"city_any": func(v string) (filterFn, bool) {
			cities := map[uint16]struct{}{}
			for _, n := range strings.Split(v, ",") {
				cid := wordbook.City.Get(n)
				if cid == 0 {
					continue
				}
				cities[cid] = struct{}{}
			}
			return func(acc *model.DBItem) bool {
				if _, ok := cities[acc.City]; ok {
					return true
				}
				return false
			}, true
		},
		"city_null_1": func(v string) (filterFn, bool) {
			return func(acc *model.DBItem) bool {
				return acc.City == 0
			}, true
		},
		"city_null_0": func(v string) (filterFn, bool) {
			return func(acc *model.DBItem) bool {
				return acc.City != 0
			}, true
		},
		"birth_gt": func(v string) (filterFn, bool) {
			b, err := strconv.Atoi(v)
			if err != nil {
				return nil, false
			}
			birth := int32(b)
			return func(acc *model.DBItem) bool {
				return acc.Birth > birth
			}, true
		},
		"birth_lt": func(v string) (filterFn, bool) {
			b, err := strconv.Atoi(v)
			if err != nil {
				return nil, false
			}
			birth := int32(b)
			return func(acc *model.DBItem) bool {
				return acc.Birth < birth
			}, true
		},
		"birth_year": func(v string) (filterFn, bool) {
			year, err := time.Parse("2006", v)
			if err != nil {
				return nil, false
			}
			start := int32(year.Unix())
			finish := int32(year.AddDate(1, 0, 0).Unix())
			return func(acc *model.DBItem) bool {
				return acc.Birth >= start && acc.Birth <= finish
			}, true
		},
		"joined_year": func(v string) (filterFn, bool) {
			// year, err := time.Parse("2006", v)
			// if err != nil {
			// 	return nil, false
			// }
			year, err := strconv.Atoi(v)
			if err != nil {
				return nil, false
			}
			// start := int32(year.Unix())
			// finish := int32(year.AddDate(1, 0, 0).Nanosecond() / 1e9)
			return func(acc *model.DBItem) bool {
				return time.Unix(int64(acc.Joined), 0).Year() == year
			}, true
		},
		"interests_contains": func(v string) (filterFn, bool) {
			interests := map[uint8]struct{}{}
			for _, n := range strings.Split(v, ",") {
				interest := wordbook.Interests.Get(n)
				if interest == 0 {
					return nil, false
				}
				interests[interest] = struct{}{}
			}
			return func(acc *model.DBItem) bool {
				if len(acc.Interests) < len(interests) {
					return false
				}
				for in := range interests {
					if _, ok := acc.Interests[in]; ok {
						continue
					}
					return false
				}
				return true
			}, true
		},
		"interests_any": func(v string) (filterFn, bool) {
			interests := map[uint8]struct{}{}
			for _, n := range strings.Split(v, ",") {
				if id := wordbook.Interests.Get(n); id > 0 {
					interests[id] = struct{}{}
				}
			}
			return func(acc *model.DBItem) bool {
				for in := range interests {
					if _, ok := acc.Interests[in]; ok {
						return true
					}
				}
				return false
			}, true
		},
		"premium_now": func(v string) (filterFn, bool) {
			return func(acc *model.DBItem) bool {
				return acc.Premium.Finish > timestamp
			}, true
		},
		"premium_null_1": func(v string) (filterFn, bool) {
			return func(acc *model.DBItem) bool {
				return acc.Premium.Shift == 0
			}, true
		},
		"premium_null_0": func(v string) (filterFn, bool) {
			return func(acc *model.DBItem) bool {
				return acc.Premium.Shift > 0
			}, true
		},
	}
)
