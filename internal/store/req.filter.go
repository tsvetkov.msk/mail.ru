package store

import (
	"context"
	"hlc/internal/model"
	"net/url"
)

// Filter -
func Filter(ctx context.Context, query url.Values, limit int) ([]map[string]interface{}, error) {
	if query.Get("likes_contains") != "" {
		return likeContainsCursor.Filter(ctx, query, limit)
	}
	if query.Get("phone_code") != "" {
		return phoneCursor.Filter(ctx, query, limit)
	}
	if query.Get("city_eq") != "" {
		return cityCursor.Filter(ctx, query, limit)
	}
	if query.Get("sname_eq") != "" {
		return snameCursor.Equal(ctx, query, limit)
	}
	if query.Get("sname_starts") != "" {
		return snameCursor.Starts(ctx, query, limit)
	}
	if query.Get("fname_eq") != "" {
		return fnameCursor.Equal(ctx, query, limit)
	}
	if query.Get("country_eq") != "" {
		return countryCursor.Filter(ctx, query, limit)
	}
	if query.Get("interests_contains") != "" {
		return interestsCursor.Filter(ctx, query, limit)
	}
	if query.Get("birth_year") != "" {
		return birthCursor.Filter(ctx, query, limit)
	}
	if query.Get("fname_any") != "" {
		return fnameCursor.Any(ctx, query, limit)
	}
	if query.Get("city_any") != "" {
		return cityCursor.Any(ctx, query, limit)
	}
	if query.Get("city_null") == "1" {
		return cityCursor.Null(ctx, query, limit)
	}
	if query.Get("country_null") == "1" {
		return countryCursor.Null(ctx, query, limit)
	}
	return sexStatusCursor.Filter(ctx, query, limit)
}

func getResponseItem(acc *model.DBItem, query url.Values) map[string]interface{} {
	if len(query) == 0 {
		return map[string]interface{}{
			"id":    acc.ID,
			"email": wordbook.Email.Conv(acc.Email),
		}
	}

	item := map[string]interface{}{
		"id":    acc.ID,
		"email": wordbook.Email.Conv(acc.Email),
	}
	for key, value := range query {
		switch key {
		case "sex_eq":
			item["sex"] = wordbook.Sex.Conv(acc.Sex)
		case "status_eq", "status_neq":
			item["status"] = wordbook.Status.Conv(acc.Status)
		case "fname_eq", "fname_any", "fname_null":
			if value[0] == "1" {
				continue
			}
			item["fname"] = wordbook.FName.Conv(acc.Fname)
		case "sname_eq", "sname_starts", "sname_null":
			if value[0] == "1" {
				continue
			}
			item["sname"] = wordbook.SName.Conv(acc.Sname)
		case "phone_code", "phone_null":
			if value[0] == "1" {
				continue
			}
			item["phone"] = acc.Phone
		case "country_eq", "country_null":
			if value[0] == "1" {
				continue
			}
			item["country"] = wordbook.Country.Conv(acc.Country)
		case "city_eq", "city_any", "city_null":
			if value[0] == "1" {
				continue
			}
			item["city"] = wordbook.City.Conv(acc.City)
		case "birth_year", "birth_gt", "birth_lt":
			item["birth"] = acc.Birth
		case "premium_now":
			item["premium"] = wordbook.Premium.Get(acc.Premium)
		case "premium_null":
			if value[0] == "1" {
				continue
			}
			item["premium"] = wordbook.Premium.Get(acc.Premium)
		}
	}
	return item
}
