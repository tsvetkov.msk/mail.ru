package store

import (
	"hlc/internal/model"
	"sync"
)

// Create -
func Create(input model.Account) error {

	outgoingLikes := likesMatcher(input.Likes)
	if err := isAccExists(outgoingLikes...); err != nil {
		return err
	}

	if _, exists := getEmail(input.Email); exists {
		return ErrEmailExists
	}
	if input.Phone != "" {
		if _, exists := getPhone(input.Phone); exists {
			return ErrPhoneExists
		}
		setPhone(input.Phone, input.ID)
	}
	setEmail(wordbook.Email.Set(input.Email), input.ID)

	batch := (input.ID - 1) / batchSize
	place := (input.ID - 1) % batchSize

	database.Lock()
	{
		for int(batch) >= len(accounts) {
			accounts = append(accounts, [batchSize]model.DBItem{})
			batchAccess = append(batchAccess, sync.RWMutex{})
		}
	}
	database.Unlock()

	batchAccess[batch].Lock()

	go func() {
		defer batchAccess[batch].Unlock()

		account := &accounts[batch][place]

		account.ID = input.ID
		account.Phone = input.Phone
		account.Birth = input.Birth
		account.Joined = input.Joined
		account.Sex = wordbook.Sex.Get(input.Gender)
		account.Status = wordbook.Status.Get(input.Status)
		account.Sname = wordbook.SName.Set(input.SName)
		account.Fname = wordbook.FName.Set(input.FName)
		account.City = wordbook.City.Set(input.City)
		account.Country = wordbook.Country.Set(input.Country)
		account.Email = wordbook.Email.Set(input.Email)
		account.Premium = wordbook.Premium.Set(input.Premium.Start, input.Premium.Finish)
		account.Outgoing = outgoingLikes
		account.Incoming = make([]int32, 0)
		account.Interests = interestMatcher(input.Interests)

		for _, like := range outgoingLikes {

			bid := (like - 1) / batchSize
			pid := (like - 1) % batchSize

			acc := &accounts[bid][pid]

			if bid == batch {
				acc.Incoming = append(acc.Incoming, account.ID)
				continue
			}

			batchAccess[bid].Lock()
			acc.Incoming = append(acc.Incoming, account.ID)
			batchAccess[bid].Unlock()
		}

		updater <- account
	}()

	return nil
}
