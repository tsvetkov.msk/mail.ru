package store

import (
	"archive/zip"
	"encoding/json"
	"fmt"
	"hlc/internal/model"
	"path"
	"runtime"
	"runtime/debug"
	"sync"
)

// Open -
func Open(filePath string) error {

	r, err := zip.OpenReader(path.Join(filePath, "data.zip"))
	if err != nil {
		return err
	}

	storageSize = len(r.File)
	batchAccess = make([]sync.RWMutex, storageSize, storageSize+3)
	accounts = make([][batchSize]model.DBItem, storageSize, storageSize+3)

	for i, f := range r.File {

		file, err := f.Open()
		if err != nil {
			return err
		}

		accs := map[string][]model.Account{}
		if err := json.NewDecoder(file).Decode(&accs); err != nil {
			return err
		}

		for _, acc := range accs["accounts"] {

			batch := (acc.ID - 1) / batchSize
			place := (acc.ID - 1) % batchSize

			accounts[batch][place].ID = acc.ID
			accounts[batch][place].Phone = acc.Phone
			accounts[batch][place].Birth = acc.Birth
			accounts[batch][place].Joined = acc.Joined
			accounts[batch][place].Sex = wordbook.Sex.Set(acc.Gender)
			accounts[batch][place].Status = wordbook.Status.Set(acc.Status)
			accounts[batch][place].Sname = wordbook.SName.Set(acc.SName)
			accounts[batch][place].Fname = wordbook.FName.Set(acc.FName)
			accounts[batch][place].City = wordbook.City.Set(acc.City)
			accounts[batch][place].Country = wordbook.Country.Set(acc.Country)
			accounts[batch][place].Email = wordbook.Email.Set(acc.Email)
			accounts[batch][place].Premium = wordbook.Premium.Set(acc.Premium.Start, acc.Premium.Finish)
			accounts[batch][place].Outgoing = likesMatcher(acc.Likes)
			accounts[batch][place].Incoming = make([]int32, 0)
			accounts[batch][place].Interests = interestMatcher(acc.Interests)
		}

		file.Close()
		runtime.GC()
		debug.FreeOSMemory()
		fmt.Printf("%d..", i)
	}

	r.Close()

	runtime.GC()
	debug.FreeOSMemory()
	printMemUsage("Loaded")

	return nil
}

func printMemUsage(args ...interface{}) {
	if len(args) > 0 {
		fmt.Print(args...)
		fmt.Print("\t")
	}
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	fmt.Printf("Alloc = %v MiB", bToMb(m.Alloc))
	fmt.Printf("\tSys = %v MiB\n", bToMb(m.Sys))
}

func bToMb(b uint64) uint64 {
	return b / 1024 / 1024
}
