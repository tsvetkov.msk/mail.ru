package store

import (
	"context"
	"hlc/internal/model"
	"hlc/internal/utils"
	"net/url"
	"sort"
	"sync"
)

type idxSexStatus struct {
	access []sync.Mutex
	dirty  []map[uint8]bool
	store  []map[uint8][]*model.DBItem
}

func (idx *idxSexStatus) Upload() {
	for i, batch := range accounts {
		for j, acc := range batch {
			batch := (acc.ID - 1) / batchSize
			for len(idx.store) <= int(batch) {
				idx.access = append(idx.access, sync.Mutex{})
				idx.dirty = append(idx.dirty, map[uint8]bool{})
				idx.store = append(idx.store, map[uint8][]*model.DBItem{})
			}
			sexStatus := acc.Sex | acc.Status
			if idx.store[batch][sexStatus] == nil {
				idx.store[batch][sexStatus] = make([]*model.DBItem, 0)
				idx.dirty[batch][sexStatus] = true
			}
			idx.store[batch][sexStatus] = append(idx.store[batch][sexStatus], &accounts[i][j])
		}
	}
}

func (idx *idxSexStatus) Update(acc *model.DBItem, sex, status uint8) {

	batch := (acc.ID - 1) / batchSize

	idx.access[batch].Lock()
	defer idx.access[batch].Unlock()

	sexStatus := acc.Sex | acc.Status
	for i, user := range idx.store[batch][sexStatus] {
		if user.ID == acc.ID {
			idx.store[batch][sexStatus] = append(idx.store[batch][sexStatus][:i], idx.store[batch][sexStatus][i+1:]...)
			break
		}
	}

	sexStatus = sex | status
	if idx.store[batch][sexStatus] == nil {
		idx.store[batch][sexStatus] = make([]*model.DBItem, 0)
	}
	idx.dirty[batch][sexStatus] = true
	idx.store[batch][sexStatus] = append(idx.store[batch][sexStatus], acc)
}

func (idx *idxSexStatus) Insert(acc *model.DBItem) {

	batch := (acc.ID - 1) / batchSize

	for len(idx.store) <= int(batch) {
		idx.access = append(idx.access, sync.Mutex{})
		idx.dirty = append(idx.dirty, map[uint8]bool{})
		idx.store = append(idx.store, map[uint8][]*model.DBItem{})
	}

	idx.access[batch].Lock()
	defer idx.access[batch].Unlock()

	sexStatus := acc.Sex | acc.Status
	idx.dirty[batch][sexStatus] = true
	idx.store[batch][sexStatus] = append(idx.store[batch][sexStatus], acc)
}

func (idx *idxSexStatus) Construct() {
	for batch := range idx.store {
		idx.access[batch].Lock()
		{
			for sest := range idx.store[batch] {
				if idx.dirty[batch][sest] {
					sort.Slice(idx.store[batch][sest], func(i, j int) bool {
						return idx.store[batch][sest][i].ID > idx.store[batch][sest][j].ID
					})
				}
				idx.dirty[batch][sest] = false
			}
		}
		idx.access[batch].Unlock()
	}
}

func (idx *idxSexStatus) Filter(ctx context.Context, query url.Values, limit int) ([]map[string]interface{}, error) {

	filters, ok := createFilters(query, "sex_eq", "status_eq", "status_neq")
	if !ok {
		// один из фильтров всегда false
		return emptyList, nil
	}

	done := make(chan bool, 0)
	defer close(done)
	accs := make([]map[string]interface{}, 0, limit)

	sex := wordbook.Sex.Get(query.Get("sex_eq"))
	equal := true
	status := wordbook.Status.Get(query.Get("status_eq"))
	if status == 0 {
		status = wordbook.Status.Get(query.Get("status_neq"))
		equal = false
	}

	for i := len(idx.store) - 1; i >= 0; i-- {
		batches := [][]*model.DBItem{}

		for sest := range idx.store[i] {
			if len(idx.store[i][sest]) == 0 {
				continue
			}
			if sex == 0 || sex&sest > 0 {
				if status == 0 {
					batches = append(batches, idx.store[i][sest])
					continue
				}
				if (equal && status&sest > 0) || (!equal && status&sest == 0) {
					batches = append(batches, idx.store[i][sest])
				}
			}
		}

		for acc := range utils.MergeHeap(done, batches...) {
			select {
			case <-ctx.Done():
				return accs, ctx.Err()
			default:
			}

			if ok := applyFilters(acc, filters); !ok {
				continue
			}

			accs = append(accs, getResponseItem(acc, query))
			if len(accs) == limit {
				return accs, nil
			}
		}
	}

	return accs, nil
}

func (idx *idxSexStatus) Iterator(done <-chan bool) <-chan *model.DBItem {
	ch := make(chan *model.DBItem, 1)
	go func() {
		defer close(ch)
	}()
	return ch
}

func (idx *idxSexStatus) Scan(query url.Values) <-chan *model.DBItem {
	ch := make(chan *model.DBItem, 1)
	go func() {

		defer close(ch)

		batches := [][]*model.DBItem{}

		if len(query) > 1 {
			return
		}

		sex := wordbook.Sex.Get(query.Get("sex"))
		status := wordbook.Status.Get(query.Get("status"))

		for i := len(idx.store) - 1; i >= 0; i-- {
			for sest := range idx.store[i] {
				if len(idx.store[i][sest]) == 0 {
					continue
				}
				if sex == 0 || sex&sest > 0 {
					if status == 0 {
						batches = append(batches, idx.store[i][sest])
						continue
					}
					if status&sest > 0 {
						batches = append(batches, idx.store[i][sest])
					}
				}
			}
		}

		done := make(chan bool, 0)
		defer close(done)

		for acc := range utils.MergeHeap(done, batches...) {
			// if ok := applyFilters(acc, filters); !ok {
			// 	continue
			// }

			ch <- acc
		}

	}()
	return ch
}

func (idx *idxSexStatus) Stat(query url.Values, key string) (groupCounter, bool) {

	if len(query) == 1 {
		switch key {
		case "country":
			if sex := query.Get("sex"); sex != "" {
				gender := wordbook.Sex.Get(sex)
				res := map[uint8]int{}
				for id, batch := range countryCursor.store {
					for g, list := range batch {
						if g&gender > 0 {
							res[id] += len(list)
						}
					}
				}
				return &groupCountryCounter{
					store: res,
				}, true
			}
			if status := query.Get("status"); status != "" {
				sid := wordbook.Status.Get(status)
				res := map[uint8]int{}
				for id, batch := range countryCursor.store {
					for s, list := range batch {
						if s&sid > 0 {
							res[id] += len(list)
						}
					}
				}
				return &groupCountryCounter{
					store: res,
				}, true
			}
		case "city":
			if sex := query.Get("sex"); sex != "" {
				gender := wordbook.Sex.Get(sex)
				res := map[uint16]int{}
				for id, batch := range cityCursor.store {
					for g, list := range batch {
						if g&gender > 0 {
							res[id] += len(list)
						}
					}
				}
				return &groupCityCounter{
					store: res,
				}, true
			}
			if status := query.Get("status"); status != "" {
				sid := wordbook.Status.Get(status)
				res := map[uint16]int{}
				for id, batch := range cityCursor.store {
					for s, list := range batch {
						if s&sid > 0 {
							res[id] += len(list)
						}
					}
				}
				return &groupCityCounter{
					store: res,
				}, true
			}
		case "city,sex":
			if sex := query.Get("sex"); sex != "" {
				res := map[uint16]map[uint8]int{}
				gender := wordbook.Sex.Get(sex)
				for id, batch := range cityCursor.store {
					if res[id] == nil {
						res[id] = map[uint8]int{}
					}
					for sex, list := range batch {
						if sex&gender > 0 {
							res[id][gender] += len(list)
						}
					}
				}
				return &groupCitySexCounter{
					store: res,
				}, true
			}
			if status := query.Get("status"); status != "" {
				res := map[uint16]map[uint8]int{}
				gender := wordbook.Status.Get(status)
				for id, batch := range cityCursor.store {
					if res[id] == nil {
						res[id] = map[uint8]int{1: 0, 2: 0}
					}
					for status, list := range batch {
						if status&gender > 0 {
							if status&1 > 0 {
								res[id][1] += len(list)
							}
							if status&2 > 0 {
								res[id][2] += len(list)
							}
						}
					}
				}
				return &groupCitySexCounter{
					store: res,
				}, true
			}
		case "city,status":
			if status := query.Get("status"); status != "" {
				res := map[uint16]map[uint8]int{}
				gender := wordbook.Status.Get(status)
				for id, batch := range cityCursor.store {
					if res[id] == nil {
						res[id] = map[uint8]int{}
					}
					for status, list := range batch {
						if status&gender > 0 {
							res[id][gender] += len(list)
						}
					}
				}
				return &groupCityStatusCounter{
					store: res,
				}, true
			}
			if sex := query.Get("sex"); sex != "" {
				res := map[uint16]map[uint8]int{}
				gender := wordbook.Sex.Get(sex)
				for id, batch := range cityCursor.store {
					if res[id] == nil {
						res[id] = map[uint8]int{4: 0, 8: 0, 16: 0}
					}
					for sex, list := range batch {
						if sex&gender > 0 {
							if sex&4 > 0 {
								res[id][4] += len(list)
							}
							if sex&8 > 0 {
								res[id][8] += len(list)
							}
							if sex&16 > 0 {
								res[id][16] += len(list)
							}
						}
					}
				}
				return &groupCityStatusCounter{
					store: res,
				}, true
			}
		case "country,status":
			if status := query.Get("status"); status != "" {
				res := map[uint8]map[uint8]int{}
				gender := wordbook.Status.Get(status)
				for id, batch := range countryCursor.store {
					if res[id] == nil {
						res[id] = map[uint8]int{}
					}
					for status, list := range batch {
						if status&gender > 0 {
							res[id][gender] += len(list)
						}
					}
				}
				return &groupCountryStatusCounter{
					store: res,
				}, true
			}
			if sex := query.Get("sex"); sex != "" {
				res := map[uint8]map[uint8]int{}
				gender := wordbook.Sex.Get(sex)
				for id, batch := range countryCursor.store {
					if res[id] == nil {
						res[id] = map[uint8]int{4: 0, 8: 0, 16: 0}
					}
					for sex, list := range batch {
						if sex&gender > 0 {
							if sex&4 > 0 {
								res[id][4] += len(list)
							}
							if sex&8 > 0 {
								res[id][8] += len(list)
							}
							if sex&16 > 0 {
								res[id][16] += len(list)
							}
						}
					}
				}
				return &groupCountryStatusCounter{
					store: res,
				}, true
			}
		case "country,sex":
			if status := query.Get("status"); status != "" {
				res := map[uint8]map[uint8]int{}
				gender := wordbook.Status.Get(status)
				for id, batch := range countryCursor.store {
					if res[id] == nil {
						res[id] = map[uint8]int{1: 0, 2: 0}
					}
					for status, list := range batch {
						if status&gender > 0 {
							if status&1 > 0 {
								res[id][1] += len(list)
							}
							if status&2 > 0 {
								res[id][2] += len(list)
							}
						}
					}
				}
				return &groupCountrySexCounter{
					store: res,
				}, true
			}
		}
		return nil, false
	}

	if len(query) > 0 {
		return nil, false
	}

	if key == "sex" {
		res := map[uint8]int{
			1: 0, 2: 0,
		}
		for _, batch := range idx.store {
			for k, v := range batch {
				if k&1 > 0 {
					res[1] += len(v)
				}
				if k&2 > 0 {
					res[2] += len(v)
				}
			}
		}
		return &groupSexCounter{
			store: res,
		}, true
	}
	if key == "status" {
		res := map[uint8]int{
			4: 0, 8: 0, 16: 0,
		}
		for _, batch := range idx.store {
			for k, v := range batch {
				if k&4 > 0 {
					res[4] += len(v)
				}
				if k&8 > 0 {
					res[8] += len(v)
				}
				if k&16 > 0 {
					res[16] += len(v)
				}
			}
		}
		return &groupStatusCounter{
			store: res,
		}, true
	}
	if key == "interests" {
		res := map[uint8]int{}
		for id, batch := range interestsCursor.store {
			res[id] = len(batch)
		}
		return &groupInterestsCounter{
			store: res,
		}, true
	}

	if key == "city" {
		res := map[uint16]int{}
		for id, batch := range cityCursor.store {
			for _, list := range batch {
				res[id] += len(list)
			}
		}
		return &groupCityCounter{
			store: res,
		}, true
	}

	if key == "country" {
		res := map[uint8]int{}
		for id, batch := range countryCursor.store {
			for _, list := range batch {
				res[id] += len(list)
			}
		}
		return &groupCountryCounter{
			store: res,
		}, true
	}

	if key == "city,sex" {
		res := map[uint16]map[uint8]int{}
		for id, batch := range cityCursor.store {
			if res[id] == nil {
				res[id] = map[uint8]int{}
			}
			for sex, list := range batch {
				if sex&1 > 0 {
					res[id][1] += len(list)
				}
				if sex&2 > 0 {
					res[id][2] += len(list)
				}
			}
		}
		return &groupCitySexCounter{
			store: res,
		}, true
	}
	if key == "country,sex" {
		res := map[uint8]map[uint8]int{}
		for id, batch := range countryCursor.store {
			if res[id] == nil {
				res[id] = map[uint8]int{}
			}
			for sex, list := range batch {
				if sex&1 > 0 {
					res[id][1] += len(list)
				}
				if sex&2 > 0 {
					res[id][2] += len(list)
				}
			}
		}
		return &groupCountrySexCounter{
			store: res,
		}, true
	}
	if key == "country,status" {
		res := map[uint8]map[uint8]int{}
		for id, batch := range countryCursor.store {
			if res[id] == nil {
				res[id] = map[uint8]int{}
			}
			for sex, list := range batch {
				if sex&4 > 0 {
					res[id][4] += len(list)
				}
				if sex&8 > 0 {
					res[id][8] += len(list)
				}
				if sex&16 > 0 {
					res[id][16] += len(list)
				}
			}
		}
		return &groupCountryStatusCounter{
			store: res,
		}, true
	}
	if key == "city,status" {
		res := map[uint16]map[uint8]int{}
		for id, batch := range cityCursor.store {
			if res[id] == nil {
				res[id] = map[uint8]int{}
			}
			for sex, list := range batch {
				if sex&4 > 0 {
					res[id][4] += len(list)
				}
				if sex&8 > 0 {
					res[id][8] += len(list)
				}
				if sex&16 > 0 {
					res[id][16] += len(list)
				}
			}
		}
		return &groupCityStatusCounter{
			store: res,
		}, true
	}

	return nil, false
}
