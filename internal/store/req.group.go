package store

import (
	"context"
	"fmt"
	"hlc/internal/model"
	"net/url"
	"sort"
	"sync"
	"time"
)

var (
	emptyGroup = []GroupItem{}
)

// Group -
func Group(query url.Values, limit int) ([]GroupItem, error) {

	desc := (query.Get("order") == "1")
	keys := query.Get("keys")
	var iterator idxIterator
	var counter groupCounter

	query.Del("keys")
	query.Del("order")

	if query.Get("birth") != "" {
		if g, ok := birthCursor.Stat(query, keys); ok {
			return g.Result(desc, limit)
		}
		//iterator = birthCursor
	}
	if query.Get("interests") != "" {
		if groups, ok := interestsCursor.Stat(query, keys); ok {
			return groups.Result(desc, limit)
		}
		iterator = interestsCursor
	}
	if query.Get("country") != "" {
		if g, ok := countryCursor.Stat(query, keys); ok {
			return g.Result(desc, limit)
		}
		iterator = countryCursor
	}
	if query.Get("city") != "" {
		if g, ok := cityCursor.Stat(query, keys); ok {
			return g.Result(desc, limit)
		}
		iterator = cityCursor
	}
	if query.Get("likes") != "" {
		iterator = likeContainsCursor
	}
	if query.Get("joined") != "" {
		if g, ok := joinedCursor.Stat(query, keys); ok {
			return g.Result(desc, limit)
		}
	}

	if iterator == nil {
		if g, ok := sexStatusCursor.Stat(query, keys); ok {
			return g.Result(desc, limit)
		}
		return emptyGroup, nil
	}

	switch keys {
	case "country":
		counter = &groupCountryCounter{
			store: make(map[uint8]int),
		}
	case "city":
		counter = &groupCityCounter{
			store: make(map[uint16]int),
		}
	case "country,status":
		counter = &groupCountryStatusCounter{
			store: make(map[uint8]map[uint8]int),
		}
	case "country,sex":
		counter = &groupCountrySexCounter{
			store: make(map[uint8]map[uint8]int),
		}
	case "city,status":
		counter = &groupCityStatusCounter{
			store: make(map[uint16]map[uint8]int),
		}
	case "city,sex":
		counter = &groupCitySexCounter{
			store: make(map[uint16]map[uint8]int),
		}
	case "sex":
		counter = &groupSexCounter{
			store: make(map[uint8]int),
		}
	case "status":
		counter = &groupStatusCounter{
			store: make(map[uint8]int),
		}
	case "interests":
		counter = &groupInterestsCounter{
			store: make(map[uint8]int),
		}
	}

	if counter == nil {
		fmt.Printf("Unknown group keypair %s\n", query.Get("keys"))
		return emptyGroup, nil
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond*1800)
	defer cancel()

	for acc := range iterator.Scan(query) {
		select {
		case <-ctx.Done():
			return emptyGroup, ctx.Err()
		default:
			counter.Count(acc)
		}
	}

	return counter.Result(desc, limit)
}

// GroupItem -
type GroupItem struct {
	Count     int    `json:"count,omitempty"`
	Sex       string `json:"sex,omitempty"`
	Status    string `json:"status,omitempty"`
	Interests string `json:"interests,omitempty"`
	Country   string `json:"country,omitempty"`
	City      string `json:"city,omitempty"`
}

type idxIterator interface {
	Scan(url.Values) <-chan *model.DBItem
}

type groupCounter interface {
	Count(*model.DBItem)
	Incr(...int)
	Decr(...int)
	Result(bool, int) ([]GroupItem, error)
}

type groupCountryCounter struct {
	access sync.Mutex
	store  map[uint8]int
}

func (gc *groupCountryCounter) Count(acc *model.DBItem) {
	gc.access.Lock()
	defer gc.access.Unlock()
	gc.store[acc.Country]++
}
func (gc *groupCountryCounter) Decr(ids ...int) {
	gc.access.Lock()
	defer gc.access.Unlock()
	gc.store[uint8(ids[0])]--
}
func (gc *groupCountryCounter) Incr(ids ...int) {
	gc.access.Lock()
	defer gc.access.Unlock()
	gc.store[uint8(ids[0])]++
}
func (gc *groupCountryCounter) Result(desc bool, limit int) (groups []GroupItem, err error) {

	for k, v := range gc.store {
		if v == 0 {
			continue
		}
		groups = append(groups, GroupItem{
			Count:   v,
			Country: wordbook.Country.Conv(k),
		})
	}

	if len(groups) == 0 {
		return emptyGroup, err
	}

	sort.Slice(groups, func(i, j int) bool {
		if desc {
			if groups[i].Count == groups[j].Count {
				return groups[i].Country < groups[j].Country
			}
			return groups[i].Count < groups[j].Count
		}
		if groups[i].Count == groups[j].Count {
			return groups[i].Country > groups[j].Country
		}
		return groups[i].Count > groups[j].Count
	})

	if len(groups) < limit {
		limit = len(groups)
	}

	return groups[:limit], err
}

type groupCountryStatusCounter struct {
	access sync.Mutex
	store  map[uint8]map[uint8]int
}

func (gc *groupCountryStatusCounter) Count(acc *model.DBItem) {
	gc.access.Lock()
	defer gc.access.Unlock()
	if gc.store[acc.Country] == nil {
		gc.store[acc.Country] = make(map[uint8]int)
	}
	gc.store[acc.Country][acc.Status]++
}
func (gc *groupCountryStatusCounter) Decr(ids ...int) {
	gc.access.Lock()
	defer gc.access.Unlock()
	if gc.store[uint8(ids[0])] == nil {
		gc.store[uint8(ids[0])] = make(map[uint8]int)
	}
	gc.store[uint8(ids[0])][uint8(ids[1])]--
}
func (gc *groupCountryStatusCounter) Incr(ids ...int) {
	gc.access.Lock()
	defer gc.access.Unlock()
	if gc.store[uint8(ids[0])] == nil {
		gc.store[uint8(ids[0])] = make(map[uint8]int)
	}
	gc.store[uint8(ids[0])][uint8(ids[1])]++
}
func (gc *groupCountryStatusCounter) Result(desc bool, limit int) (groups []GroupItem, err error) {

	for country, batch := range gc.store {
		for status, v := range batch {
			if v == 0 {
				continue
			}
			groups = append(groups, GroupItem{
				Count:   v,
				Status:  wordbook.Status.Conv(status),
				Country: wordbook.Country.Conv(country),
			})
		}
	}

	if len(groups) == 0 {
		return emptyGroup, err
	}

	sort.Slice(groups, func(i, j int) bool {
		if desc {
			if groups[i].Count == groups[j].Count {
				if groups[i].Country == groups[j].Country {
					return groups[i].Status < groups[j].Status
				}
				return groups[i].Country < groups[j].Country
			}
			return groups[i].Count < groups[j].Count
		}
		if groups[i].Count == groups[j].Count {
			if groups[i].Country == groups[j].Country {
				return groups[i].Status > groups[j].Status
			}
			return groups[i].Country > groups[j].Country
		}
		return groups[i].Count > groups[j].Count
	})

	if len(groups) < limit {
		limit = len(groups)
	}

	return groups[:limit], err
}

type groupCountrySexCounter struct {
	access sync.Mutex
	store  map[uint8]map[uint8]int
}

func (gc *groupCountrySexCounter) Count(acc *model.DBItem) {
	gc.access.Lock()
	defer gc.access.Unlock()
	if gc.store[acc.Country] == nil {
		gc.store[acc.Country] = make(map[uint8]int)
	}
	gc.store[acc.Country][acc.Sex]++
}
func (gc *groupCountrySexCounter) Decr(ids ...int) {
	gc.access.Lock()
	defer gc.access.Unlock()
	if gc.store[uint8(ids[0])] == nil {
		gc.store[uint8(ids[0])] = make(map[uint8]int)
	}
	gc.store[uint8(ids[0])][uint8(ids[1])]--
}
func (gc *groupCountrySexCounter) Incr(ids ...int) {
	gc.access.Lock()
	defer gc.access.Unlock()
	if gc.store[uint8(ids[0])] == nil {
		gc.store[uint8(ids[0])] = make(map[uint8]int)
	}
	gc.store[uint8(ids[0])][uint8(ids[1])]++
}
func (gc *groupCountrySexCounter) Result(desc bool, limit int) (groups []GroupItem, err error) {

	for country, batch := range gc.store {
		for sex, v := range batch {
			if v == 0 {
				continue
			}
			groups = append(groups, GroupItem{
				Count:   v,
				Sex:     wordbook.Sex.Conv(sex),
				Country: wordbook.Country.Conv(country),
			})
		}
	}

	if len(groups) == 0 {
		return emptyGroup, err
	}

	sort.Slice(groups, func(i, j int) bool {
		if desc {
			if groups[i].Count == groups[j].Count {
				if groups[i].Country == groups[j].Country {
					return groups[i].Sex < groups[j].Sex
				}
				return groups[i].Country < groups[j].Country
			}
			return groups[i].Count < groups[j].Count
		}
		if groups[i].Count == groups[j].Count {
			if groups[i].Country == groups[j].Country {
				return groups[i].Sex > groups[j].Sex
			}
			return groups[i].Country > groups[j].Country
		}
		return groups[i].Count > groups[j].Count
	})

	if len(groups) < limit {
		limit = len(groups)
	}

	return groups[:limit], err
}

// CITY
type groupCityCounter struct {
	access sync.Mutex
	store  map[uint16]int
}

func (gc *groupCityCounter) Count(acc *model.DBItem) {
	gc.access.Lock()
	defer gc.access.Unlock()
	gc.store[acc.City]++
}
func (gc *groupCityCounter) Decr(ids ...int) {
	gc.access.Lock()
	defer gc.access.Unlock()
	gc.store[uint16(ids[0])]--
}
func (gc *groupCityCounter) Incr(ids ...int) {
	gc.access.Lock()
	defer gc.access.Unlock()
	gc.store[uint16(ids[0])]++
}
func (gc *groupCityCounter) Result(desc bool, limit int) (groups []GroupItem, err error) {

	for k, v := range gc.store {
		if v == 0 {
			continue
		}
		groups = append(groups, GroupItem{
			Count: v,
			City:  wordbook.City.Conv(k),
		})
	}

	if len(groups) == 0 {
		return emptyGroup, err
	}

	sort.Slice(groups, func(i, j int) bool {
		if desc {
			if groups[i].Count == groups[j].Count {
				return groups[i].City < groups[j].City
			}
			return groups[i].Count < groups[j].Count
		}
		if groups[i].Count == groups[j].Count {
			return groups[i].City > groups[j].City
		}
		return groups[i].Count > groups[j].Count
	})

	if len(groups) < limit {
		limit = len(groups)
	}

	return groups[:limit], err
}

type groupCityStatusCounter struct {
	access sync.Mutex
	store  map[uint16]map[uint8]int
}

func (gc *groupCityStatusCounter) Count(acc *model.DBItem) {
	gc.access.Lock()
	defer gc.access.Unlock()
	if gc.store[acc.City] == nil {
		gc.store[acc.City] = make(map[uint8]int)
	}
	gc.store[acc.City][acc.Status]++
}
func (gc *groupCityStatusCounter) Decr(ids ...int) {
	gc.access.Lock()
	defer gc.access.Unlock()
	if gc.store[uint16(ids[0])] == nil {
		gc.store[uint16(ids[0])] = make(map[uint8]int)
	}
	gc.store[uint16(ids[0])][uint8(ids[1])]--
}
func (gc *groupCityStatusCounter) Incr(ids ...int) {
	gc.access.Lock()
	defer gc.access.Unlock()
	if gc.store[uint16(ids[0])] == nil {
		gc.store[uint16(ids[0])] = make(map[uint8]int)
	}
	gc.store[uint16(ids[0])][uint8(ids[1])]++
}
func (gc *groupCityStatusCounter) Result(desc bool, limit int) (groups []GroupItem, err error) {

	for city, batch := range gc.store {
		for status, v := range batch {
			if v == 0 {
				continue
			}
			groups = append(groups, GroupItem{
				Count:  v,
				Status: wordbook.Status.Conv(status),
				City:   wordbook.City.Conv(city),
			})
		}
	}

	if len(groups) == 0 {
		return emptyGroup, err
	}

	sort.Slice(groups, func(i, j int) bool {
		if desc {
			if groups[i].Count == groups[j].Count {
				if groups[i].City == groups[j].City {
					return groups[i].Status < groups[j].Status
				}
				return groups[i].City < groups[j].City
			}
			return groups[i].Count < groups[j].Count
		}
		if groups[i].Count == groups[j].Count {
			if groups[i].City == groups[j].City {
				return groups[i].Status > groups[j].Status
			}
			return groups[i].City > groups[j].City
		}
		return groups[i].Count > groups[j].Count
	})

	if len(groups) < limit {
		limit = len(groups)
	}

	return groups[:limit], err
}

type groupCitySexCounter struct {
	access sync.Mutex
	store  map[uint16]map[uint8]int
}

func (gc *groupCitySexCounter) Count(acc *model.DBItem) {
	gc.access.Lock()
	defer gc.access.Unlock()
	if gc.store[acc.City] == nil {
		gc.store[acc.City] = make(map[uint8]int)
	}
	gc.store[acc.City][acc.Sex]++
}
func (gc *groupCitySexCounter) Decr(ids ...int) {
	gc.access.Lock()
	defer gc.access.Unlock()
	if gc.store[uint16(ids[0])] == nil {
		gc.store[uint16(ids[0])] = make(map[uint8]int)
	}
	gc.store[uint16(ids[0])][uint8(ids[1])]--
}
func (gc *groupCitySexCounter) Incr(ids ...int) {
	gc.access.Lock()
	defer gc.access.Unlock()
	if gc.store[uint16(ids[0])] == nil {
		gc.store[uint16(ids[0])] = make(map[uint8]int)
	}
	gc.store[uint16(ids[0])][uint8(ids[1])]++
}
func (gc *groupCitySexCounter) Result(desc bool, limit int) (groups []GroupItem, err error) {

	for city, batch := range gc.store {
		for sex, v := range batch {
			if v == 0 {
				continue
			}
			groups = append(groups, GroupItem{
				Count: v,
				Sex:   wordbook.Sex.Conv(sex),
				City:  wordbook.City.Conv(city),
			})
		}
	}

	if len(groups) == 0 {
		return emptyGroup, err
	}

	sort.Slice(groups, func(i, j int) bool {
		if desc {
			if groups[i].Count == groups[j].Count {
				if groups[i].City == groups[j].City {
					return groups[i].Sex < groups[j].Sex
				}
				return groups[i].City < groups[j].City
			}
			return groups[i].Count < groups[j].Count
		}
		if groups[i].Count == groups[j].Count {
			if groups[i].City == groups[j].City {
				return groups[i].Sex > groups[j].Sex
			}
			return groups[i].City > groups[j].City
		}
		return groups[i].Count > groups[j].Count
	})

	if len(groups) < limit {
		limit = len(groups)
	}

	return groups[:limit], err
}

type groupStatusCounter struct {
	access sync.Mutex
	store  map[uint8]int
}

func (gc *groupStatusCounter) Count(acc *model.DBItem) {
	gc.access.Lock()
	defer gc.access.Unlock()
	gc.store[acc.Status]++
}
func (gc *groupStatusCounter) Decr(ids ...int) {
	gc.access.Lock()
	defer gc.access.Unlock()
	gc.store[uint8(ids[0])]--
}
func (gc *groupStatusCounter) Incr(ids ...int) {
	gc.access.Lock()
	defer gc.access.Unlock()
	gc.store[uint8(ids[0])]++
}
func (gc *groupStatusCounter) Result(desc bool, limit int) (groups []GroupItem, err error) {

	for k, v := range gc.store {
		if v == 0 {
			continue
		}
		groups = append(groups, GroupItem{
			Count:  v,
			Status: wordbook.Status.Conv(k),
		})
	}

	if len(groups) == 0 {
		return emptyGroup, err
	}

	sort.Slice(groups, func(i, j int) bool {
		if desc {
			if groups[i].Count == groups[j].Count {
				return groups[i].Status < groups[j].Status
			}
			return groups[i].Count < groups[j].Count
		}
		if groups[i].Count == groups[j].Count {
			return groups[i].Status > groups[j].Status
		}
		return groups[i].Count > groups[j].Count
	})

	if len(groups) < limit {
		limit = len(groups)
	}

	return groups[:limit], err
}

type groupSexCounter struct {
	access sync.Mutex
	store  map[uint8]int
}

func (gc *groupSexCounter) Count(acc *model.DBItem) {
	gc.access.Lock()
	defer gc.access.Unlock()
	gc.store[acc.Sex]++
}
func (gc *groupSexCounter) Decr(ids ...int) {
	gc.access.Lock()
	defer gc.access.Unlock()
	gc.store[uint8(ids[0])]--
}
func (gc *groupSexCounter) Incr(ids ...int) {
	gc.access.Lock()
	defer gc.access.Unlock()
	gc.store[uint8(ids[0])]++
}
func (gc *groupSexCounter) Result(desc bool, limit int) (groups []GroupItem, err error) {

	for k, v := range gc.store {
		if v == 0 {
			continue
		}
		groups = append(groups, GroupItem{
			Count: v,
			Sex:   wordbook.Sex.Conv(k),
		})
	}

	if len(groups) == 0 {
		return emptyGroup, err
	}

	sort.Slice(groups, func(i, j int) bool {
		if desc {
			if groups[i].Count == groups[j].Count {
				return groups[i].Sex < groups[j].Sex
			}
			return groups[i].Count < groups[j].Count
		}
		if groups[i].Count == groups[j].Count {
			return groups[i].Sex > groups[j].Sex
		}
		return groups[i].Count > groups[j].Count
	})

	if len(groups) < limit {
		limit = len(groups)
	}

	return groups[:limit], err
}

type groupInterestsCounter struct {
	access sync.Mutex
	store  map[uint8]int
}

func (gc *groupInterestsCounter) Count(acc *model.DBItem) {
	gc.access.Lock()
	defer gc.access.Unlock()
	for k := range acc.Interests {
		gc.store[k]++
	}
}
func (gc *groupInterestsCounter) Decr(ids ...int) {
	gc.access.Lock()
	defer gc.access.Unlock()
	gc.store[uint8(ids[0])]--
}
func (gc *groupInterestsCounter) Incr(ids ...int) {
	gc.access.Lock()
	defer gc.access.Unlock()
	gc.store[uint8(ids[0])]++
}
func (gc *groupInterestsCounter) Result(desc bool, limit int) (groups []GroupItem, err error) {

	for k, v := range gc.store {
		if v == 0 {
			continue
		}
		groups = append(groups, GroupItem{
			Count:     v,
			Interests: wordbook.Interests.Conv(k),
		})
	}

	if len(groups) == 0 {
		return emptyGroup, err
	}

	sort.Slice(groups, func(i, j int) bool {
		if desc {
			if groups[i].Count == groups[j].Count {
				return groups[i].Interests < groups[j].Interests
			}
			return groups[i].Count < groups[j].Count
		}
		if groups[i].Count == groups[j].Count {
			return groups[i].Interests > groups[j].Interests
		}
		return groups[i].Count > groups[j].Count
	})

	if len(groups) < limit {
		limit = len(groups)
	}

	return groups[:limit], err
}
