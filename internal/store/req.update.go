package store

import (
	"hlc/internal/model"
)

// Update -
func Update(id int32, input model.UpdAcc) error {

	if err := isAccExists(id); err != nil {
		return err
	}

	eupd, pupd := false, false

	if input.Email != "" {
		if aid, exists := getEmail(input.Email); exists {
			if aid != id {
				return ErrEmailExists
			}
		} else {
			eupd = true
		}
	}

	if input.Phone != "" {
		if aid, exists := getPhone(input.Phone); exists {
			if aid != id {
				return ErrPhoneExists
			}
		} else {
			pupd = true
		}
	}

	batch := (id - 1) / batchSize
	place := (id - 1) % batchSize

	batchAccess[batch].Lock()

	go func() {
		defer batchAccess[batch].Unlock()

		acc := &accounts[batch][place]

		joinedCursor.UpdateStat(acc, input)

		if pupd {
			deletePhone(acc.Phone)
			setPhone(input.Phone, acc.ID)
			phoneCursor.Update(acc, acc.Phone, input.Phone)
			acc.Phone = input.Phone
		}
		if eupd {
			deleteEmail(acc.Email)
			setEmail(wordbook.Email.Set(input.Email), acc.ID)
			acc.Email = wordbook.Email.Set(input.Email)
		}

		sex, status, changed := acc.Sex, acc.Status, false

		if input.Gender != "" || input.Status != "" {
			if s := input.Status; s != "" {
				status = wordbook.Status.Get(s)
				changed = true
			}
			if s := input.Gender; s != "" {
				sex = wordbook.Sex.Get(s)
				changed = true
			}
		}

		if input.City != "" {
			city := wordbook.City.Set(input.City)
			cityCursor.Update(acc, city, sex, status)
			acc.City = city
		} else if changed {
			cityCursor.Update(acc, acc.City, sex, status)
		}

		if input.Country != "" {
			country := wordbook.Country.Set(input.Country)
			countryCursor.Update(acc, country, sex, status)
			acc.Country = country
		} else if changed {
			countryCursor.Update(acc, acc.Country, sex, status)
		}

		if input.SName != "" {
			snameCursor.Update(acc, wordbook.SName.Conv(acc.Sname), input.SName)
			acc.Sname = wordbook.SName.Set(input.SName)
		}

		if input.FName != "" {
			fnameCursor.Update(acc, acc.Fname, wordbook.FName.Set(input.FName), wordbook.Sex.Get(input.Gender))
			acc.Fname = wordbook.FName.Set(input.FName)
		}
		if input.Birth > 0 {
			birthCursor.Update(acc, acc.Birth, input.Birth, sex, status)
			acc.Birth = input.Birth
		}
		if input.Joined > 0 {
			acc.Joined = input.Joined
		}

		if len(input.Interests) > 0 {
			interests := interestMatcher(input.Interests)
			interestsCursor.Update(acc, acc.Interests, interests)
			acc.Interests = interests
		}

		if input.Premium.Finish > 0 {
			acc.Premium = wordbook.Premium.Set(input.Premium.Start, input.Premium.Finish)
		}

		if changed {
			sexStatusCursor.Update(acc, sex, status)
		}

		acc.Sex = sex
		acc.Status = status

		updater <- nil
	}()

	return nil
}
