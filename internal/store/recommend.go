package store

import (
	"hlc/internal/model"
)

// Recommend -
func Recommend(id int32, limit int) ([]map[string]interface{}, error) {
	if err := isAccExists(id); err != nil {
		return nil, err
	}

	return emptyList, nil

	// b := (id - 1) / batchSize
	// p := (id - 1) % batchSize

	// user := &accounts[b][p]

	// batches := [][]*model.DBItem{}
	// for k := range user.Interests {
	// 	batch, ok := interestsCursor.store[k]
	// 	if !ok {
	// 		return emptyList, nil
	// 	}
	// 	batches = append(batches, batch)
	// }

	// done := make(chan bool, 0)
	// defer close(done)
	// count := 0
	// last := int32(0)

	// for acc := range utils.MergeHeap(done, batches...) {
	// 	if last == acc.ID {
	// 		continue
	// 	}
	// 	if user.Sex == acc.Sex || acc.Status != 4 || acc.Premium.Shift == 0 {
	// 		continue
	// 	}
	// 	if countMatches(user, acc) == 0 {
	// 		continue
	// 	}
	// 	count++
	// 	fmt.Println(acc.ID)
	// 	last = acc.ID
	// }

	// fmt.Println(count)

	// return emptyList, nil
}

func countMatches(user, acc *model.DBItem) (count int) {
	for id := range user.Interests {
		if _, ok := acc.Interests[id]; ok {
			count++
		}
	}
	return count
}
