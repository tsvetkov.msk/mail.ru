package store

import (
	"context"
	"hlc/internal/model"
	"hlc/internal/utils"
	"net/url"
	"sort"
	"strings"
	"sync"
)

type idxFname struct {
	access sync.Mutex
	dirty  map[uint8]bool
	store  map[uint8]map[uint8][]*model.DBItem
	stat   map[uint8]map[uint8]int
}

func (idx *idxFname) Upload() {
	for i, batch := range accounts {
		for j, acc := range batch {
			if acc.Fname == 0 {
				continue
			}
			if idx.store[acc.Fname] == nil {
				idx.stat[acc.Fname] = map[uint8]int{1: 0, 2: 0}
				idx.store[acc.Fname] = make(map[uint8][]*model.DBItem, 0)
			}
			idx.dirty[acc.Fname] = true
			idx.store[acc.Fname][acc.Sex] = append(idx.store[acc.Fname][acc.Sex], &accounts[i][j])
			idx.stat[acc.Fname][acc.Sex]++
		}
	}
}

func (idx *idxFname) Insert(acc *model.DBItem) {

	if acc.Fname == 0 {
		return
	}

	idx.access.Lock()
	defer idx.access.Unlock()

	if idx.store[acc.Fname] == nil {
		idx.stat[acc.Fname] = map[uint8]int{1: 0, 2: 0}
		idx.store[acc.Fname] = make(map[uint8][]*model.DBItem, 0)
	}
	idx.dirty[acc.Fname] = true
	idx.store[acc.Fname][acc.Sex] = append(idx.store[acc.Fname][acc.Sex], acc)
	idx.stat[acc.Fname][acc.Sex]++
}

func (idx *idxFname) Update(acc *model.DBItem, from, to, sex uint8) {

	if from == to {
		return
	}

	idx.access.Lock()
	defer idx.access.Unlock()

	if from != 0 {
		for i, user := range idx.store[from][acc.Sex] {
			if user.ID == acc.ID {
				idx.store[from][acc.Sex] = append(idx.store[from][acc.Sex][:i], idx.store[from][acc.Sex][i+1:]...)
				idx.stat[from][acc.Sex]--
				break
			}
		}
	}

	if idx.store[to] == nil {
		idx.stat[to] = map[uint8]int{1: 0, 2: 0}
		idx.store[to] = make(map[uint8][]*model.DBItem, 0)
	}
	idx.dirty[to] = true
	idx.store[to][sex] = append(idx.store[to][sex], acc)
	idx.stat[to][sex]++
}

func (idx *idxFname) Construct() {

	idx.access.Lock()
	defer idx.access.Unlock()

	for fname := range idx.store {
		if idx.dirty[fname] {
			for sex := range idx.store[fname] {
				sort.Slice(idx.store[fname][sex], func(i, j int) bool {
					return idx.store[fname][sex][i].ID > idx.store[fname][sex][j].ID
				})
			}
		}
		idx.dirty[fname] = false
	}
}

func (idx *idxFname) Equal(ctx context.Context, query url.Values, limit int) ([]map[string]interface{}, error) {

	filters, ok := createFilters(query, "fname_eq")
	if !ok {
		// один из фильтров всегда false
		return emptyList, nil
	}

	fname := wordbook.FName.Get(query.Get("fname_eq"))
	if fname == 0 {
		return emptyList, nil
	}

	batches := [][]*model.DBItem{}
	sex := wordbook.Sex.Get(query.Get("sex"))

	for s, batch := range idx.store[fname] {
		if len(batch) == 0 {
			continue
		}
		if sex&s == 0 {
			continue
		}
		batches = append(batches, batch)
	}

	if len(batches) == 0 {
		return emptyList, nil
	}

	accs := make([]map[string]interface{}, 0, limit)

	done := make(chan bool, 0)
	defer close(done)

	for acc := range utils.MergeHeap(done, batches...) {
		select {
		case <-ctx.Done():
			return accs, ctx.Err()
		default:
		}

		if ok := applyFilters(acc, filters); !ok {
			continue
		}

		accs = append(accs, getResponseItem(acc, query))
		if len(accs) == limit {
			break
		}
	}

	return accs, nil
}

func (idx *idxFname) Any(ctx context.Context, query url.Values, limit int) ([]map[string]interface{}, error) {

	filters, ok := createFilters(query, "fname_any")
	if !ok {
		// один из фильтров всегда false
		return emptyList, nil
	}

	batches := [][]*model.DBItem{}
	sex := wordbook.Sex.Get(query.Get("sex"))

	for _, fname := range strings.Split(query.Get("fname_any"), ",") {
		id := wordbook.FName.Get(fname)
		if id == 0 {
			continue
		}

		for s, batch := range idx.store[wordbook.FName.Get(fname)] {
			if len(batch) == 0 {
				continue
			}
			if sex > 0 && sex&s == 0 {
				continue
			}
			batches = append(batches, batch)
		}
	}

	if len(batches) == 0 {
		return emptyList, nil
	}

	accs := make([]map[string]interface{}, 0, limit)

	done := make(chan bool, 0)
	defer close(done)

	for acc := range utils.MergeHeap(done, batches...) {
		select {
		case <-ctx.Done():
			return accs, ctx.Err()
		default:
		}

		if ok := applyFilters(acc, filters); !ok {
			continue
		}

		accs = append(accs, getResponseItem(acc, query))
		if len(accs) == limit {
			break
		}
	}

	return accs, nil
}
