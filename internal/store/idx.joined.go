package store

import (
	"hlc/internal/model"
	"net/url"
	"sync"
	"time"
)

type idxJoined struct {
	access sync.Mutex
	stat   map[string]map[string]groupCounter
	query  map[string]map[string]map[string]map[uint8]groupCounter
}

func (idx *idxJoined) Upload() {
	for _, batch := range accounts {
		for _, acc := range batch {
			year := time.Unix(int64(acc.Joined), 0).Format("2006")
			if idx.stat[year] == nil {
				idx.addBatch(year)
			}
			idx.stat[year]["sex"].Incr(int(acc.Sex))
			idx.stat[year]["status"].Incr(int(acc.Status))
			idx.stat[year]["city"].Incr(int(acc.City))
			idx.stat[year]["country"].Incr(int(acc.Country))
			idx.stat[year]["country,status"].Incr(int(acc.Country), int(acc.Status))
			idx.stat[year]["country,sex"].Incr(int(acc.Country), int(acc.Sex))
			for k := range acc.Interests {
				idx.stat[year]["interests"].Incr(int(k))
			}

			idx.query[year]["sex"]["city,status"][acc.Sex].Incr(int(acc.City), int(acc.Status))
			idx.query[year]["status"]["city,status"][acc.Status].Incr(int(acc.City), int(acc.Status))

			idx.query[year]["status"]["city,sex"][acc.Status].Incr(int(acc.City), int(acc.Sex))

			idx.query[year]["sex"]["country,sex"][acc.Sex].Incr(int(acc.Country), int(acc.Sex))
			idx.query[year]["status"]["country,sex"][acc.Status].Incr(int(acc.Country), int(acc.Sex))

			idx.query[year]["sex"]["country,status"][acc.Sex].Incr(int(acc.Country), int(acc.Status))
			idx.query[year]["status"]["country,status"][acc.Status].Incr(int(acc.Country), int(acc.Status))

			idx.query[year]["sex"]["country"][acc.Sex].Incr(int(acc.Country))
			idx.query[year]["status"]["country"][acc.Status].Incr(int(acc.Country))

			idx.query[year]["sex"]["city"][acc.Sex].Incr(int(acc.City))
			idx.query[year]["status"]["city"][acc.Status].Incr(int(acc.City))
		}
	}
}

func (idx *idxJoined) Insert(acc *model.DBItem) {

	idx.access.Lock()
	defer idx.access.Unlock()

	year := time.Unix(int64(acc.Joined), 0).Format("2006")
	if idx.stat[year] == nil {
		idx.addBatch(year)
	}
	idx.stat[year]["sex"].Incr(int(acc.Sex))
	idx.stat[year]["status"].Incr(int(acc.Status))
	idx.stat[year]["city"].Incr(int(acc.City))
	idx.stat[year]["country"].Incr(int(acc.Country))
	idx.stat[year]["country,status"].Incr(int(acc.Country), int(acc.Status))
	idx.stat[year]["country,sex"].Incr(int(acc.Country), int(acc.Sex))
	for k := range acc.Interests {
		idx.stat[year]["interests"].Incr(int(k))
	}

	idx.query[year]["sex"]["city,status"][acc.Sex].Incr(int(acc.City), int(acc.Status))
	idx.query[year]["status"]["city,status"][acc.Status].Incr(int(acc.City), int(acc.Status))

	idx.query[year]["status"]["city,sex"][acc.Status].Incr(int(acc.City), int(acc.Sex))

	idx.query[year]["sex"]["country,sex"][acc.Sex].Incr(int(acc.Country), int(acc.Sex))
	idx.query[year]["status"]["country,sex"][acc.Status].Incr(int(acc.Country), int(acc.Sex))

	idx.query[year]["sex"]["country,status"][acc.Sex].Incr(int(acc.Country), int(acc.Status))
	idx.query[year]["status"]["country,status"][acc.Status].Incr(int(acc.Country), int(acc.Status))

	idx.query[year]["sex"]["country"][acc.Sex].Incr(int(acc.Country))
	idx.query[year]["status"]["country"][acc.Status].Incr(int(acc.Country))

	idx.query[year]["sex"]["city"][acc.Sex].Incr(int(acc.City))
	idx.query[year]["status"]["city"][acc.Status].Incr(int(acc.City))
}

func (idx *idxJoined) addBatch(year string) {
	idx.stat[year] = map[string]groupCounter{
		"sex": &groupSexCounter{
			store: make(map[uint8]int),
		},
		"status": &groupStatusCounter{
			store: make(map[uint8]int),
		},
		"interests": &groupInterestsCounter{
			store: make(map[uint8]int),
		},
		"city": &groupCityCounter{
			store: make(map[uint16]int),
		},
		"country": &groupCountryCounter{
			store: make(map[uint8]int),
		},
		"country,status": &groupCountryStatusCounter{
			store: make(map[uint8]map[uint8]int),
		},
		"country,sex": &groupCountrySexCounter{
			store: make(map[uint8]map[uint8]int),
		},
	}

	idx.query[year] = map[string]map[string]map[uint8]groupCounter{
		"sex": map[string]map[uint8]groupCounter{
			"city,status": map[uint8]groupCounter{
				1: &groupCityStatusCounter{
					store: make(map[uint16]map[uint8]int),
				},
				2: &groupCityStatusCounter{
					store: make(map[uint16]map[uint8]int),
				},
			},
			"country,status": map[uint8]groupCounter{
				1: &groupCountryStatusCounter{
					store: make(map[uint8]map[uint8]int),
				},
				2: &groupCountryStatusCounter{
					store: make(map[uint8]map[uint8]int),
				},
			},
			"country,sex": map[uint8]groupCounter{
				1: &groupCountrySexCounter{
					store: make(map[uint8]map[uint8]int),
				},
				2: &groupCountrySexCounter{
					store: make(map[uint8]map[uint8]int),
				},
			},
			"country": map[uint8]groupCounter{
				1: &groupCountryCounter{
					store: make(map[uint8]int),
				},
				2: &groupCountryCounter{
					store: make(map[uint8]int),
				},
			},
			"city": map[uint8]groupCounter{
				1: &groupCityCounter{
					store: make(map[uint16]int),
				},
				2: &groupCityCounter{
					store: make(map[uint16]int),
				},
			},
		},
		"status": map[string]map[uint8]groupCounter{
			"city,status": map[uint8]groupCounter{
				4: &groupCityStatusCounter{
					store: make(map[uint16]map[uint8]int),
				},
				8: &groupCityStatusCounter{
					store: make(map[uint16]map[uint8]int),
				},
				16: &groupCityStatusCounter{
					store: make(map[uint16]map[uint8]int),
				},
			},
			"city,sex": map[uint8]groupCounter{
				4: &groupCitySexCounter{
					store: make(map[uint16]map[uint8]int),
				},
				8: &groupCitySexCounter{
					store: make(map[uint16]map[uint8]int),
				},
				16: &groupCitySexCounter{
					store: make(map[uint16]map[uint8]int),
				},
			},
			"country,status": map[uint8]groupCounter{
				4: &groupCountryStatusCounter{
					store: make(map[uint8]map[uint8]int),
				},
				8: &groupCountryStatusCounter{
					store: make(map[uint8]map[uint8]int),
				},
				16: &groupCountryStatusCounter{
					store: make(map[uint8]map[uint8]int),
				},
			},
			"country,sex": map[uint8]groupCounter{
				4: &groupCountrySexCounter{
					store: make(map[uint8]map[uint8]int),
				},
				8: &groupCountrySexCounter{
					store: make(map[uint8]map[uint8]int),
				},
				16: &groupCountrySexCounter{
					store: make(map[uint8]map[uint8]int),
				},
			},
			"country": map[uint8]groupCounter{
				4: &groupCountryCounter{
					store: make(map[uint8]int),
				},
				8: &groupCountryCounter{
					store: make(map[uint8]int),
				},
				16: &groupCountryCounter{
					store: make(map[uint8]int),
				},
			},
			"city": map[uint8]groupCounter{
				4: &groupCityCounter{
					store: make(map[uint16]int),
				},
				8: &groupCityCounter{
					store: make(map[uint16]int),
				},
				16: &groupCityCounter{
					store: make(map[uint16]int),
				},
			},
		},
	}
}

func (idx *idxJoined) Stat(query url.Values, key string) (groupCounter, bool) {

	year := query.Get("joined")
	if idx.stat[year] == nil {
		return nil, false
	}

	if len(query) == 1 {
		if groups, ok := idx.stat[year][key]; ok {
			return groups, true
		}
	}
	if len(query) == 2 {

		if sex := query.Get("sex"); sex != "" {
			if batch, ok := idx.query[year]["sex"]; ok {
				if page, ok := batch[key]; ok {
					if groups, ok := page[wordbook.Sex.Get(sex)]; ok {
						return groups, true
					}
				}
			}
		}
		if status := query.Get("status"); status != "" {
			if batch, ok := idx.query[year]["status"]; ok {
				if page, ok := batch[key]; ok {
					if groups, ok := page[wordbook.Status.Get(status)]; ok {
						return groups, true
					}
				}
			}
		}
	}
	return nil, false
}

func (idx *idxJoined) UpdateStat(acc *model.DBItem, upd model.UpdAcc) {

	prevYear := time.Unix(int64(acc.Joined), 0).Format("2006")
	nextYear := prevYear

	prevStatus := acc.Status
	nextStatus := prevStatus

	prevSex := acc.Sex
	nextSex := prevSex

	prevCountry := acc.Country
	nextCountry := prevCountry

	prevCity := acc.City
	nextCity := prevCity

	if idx.stat[nextYear] == nil {
		idx.addBatch(nextYear)
	}

	if upd.Birth != 0 {
		nextYear = time.Unix(int64(upd.Joined), 0).Format("2006")
	}

	if upd.Gender != "" {
		if sex := wordbook.Sex.Get(upd.Gender); acc.Sex != sex {
			idx.stat[prevYear]["sex"].Decr(int(acc.Sex))
			idx.stat[nextYear]["sex"].Incr(int(sex))
		}
	}
	if upd.Status != "" {
		if status := wordbook.Status.Get(upd.Status); acc.Status != status {
			idx.stat[prevYear]["status"].Decr(int(acc.Status))
			idx.stat[nextYear]["status"].Incr(int(status))
			nextStatus = status
		}
	}
	if upd.City != "" {
		if city := wordbook.City.Get(upd.City); acc.City != city {
			idx.stat[prevYear]["city"].Decr(int(acc.City))
			idx.stat[nextYear]["city"].Incr(int(city))
			nextCity = city
		}
	}
	if upd.Country != "" {
		if country := wordbook.Country.Get(upd.Country); acc.Country != country {
			idx.stat[prevYear]["country"].Decr(int(acc.Country))
			idx.stat[nextYear]["country"].Incr(int(country))
			nextCountry = country
		}
	}

	if len(upd.Interests) > 0 {
		for k := range acc.Interests {
			idx.stat[prevYear]["interests"].Decr(int(k))
		}
		for _, k := range upd.Interests {
			idx.stat[prevYear]["interests"].Incr(int(wordbook.Interests.Set(k)))
		}
	}

	idx.stat[prevYear]["country,status"].Decr(int(prevCountry), int(prevStatus))
	idx.stat[nextYear]["country,status"].Incr(int(nextCountry), int(nextStatus))

	idx.stat[prevYear]["country,sex"].Decr(int(prevCountry), int(prevSex))
	idx.stat[nextYear]["country,sex"].Incr(int(nextCountry), int(nextSex))

	idx.query[prevYear]["sex"]["city,status"][prevSex].Decr(int(prevCity), int(prevStatus))
	idx.query[nextYear]["sex"]["city,status"][nextSex].Incr(int(nextCity), int(nextStatus))

	idx.query[prevYear]["status"]["city,status"][prevStatus].Decr(int(prevCity), int(prevStatus))
	idx.query[nextYear]["status"]["city,status"][nextStatus].Incr(int(nextCity), int(nextStatus))

	idx.query[prevYear]["sex"]["country"][prevSex].Decr(int(prevCountry))
	idx.query[nextYear]["sex"]["country"][nextSex].Incr(int(nextCountry))

	idx.query[prevYear]["status"]["country"][prevStatus].Decr(int(prevCountry))
	idx.query[nextYear]["status"]["country"][nextStatus].Incr(int(nextCountry))

	idx.query[prevYear]["sex"]["city"][prevSex].Decr(int(prevCity))
	idx.query[nextYear]["sex"]["city"][nextSex].Incr(int(nextCity))

	idx.query[prevYear]["status"]["city"][prevStatus].Decr(int(prevCity))
	idx.query[nextYear]["status"]["city"][nextStatus].Incr(int(nextCity))

	idx.query[prevYear]["sex"]["country,status"][prevSex].Decr(int(prevCountry), int(prevStatus))
	idx.query[nextYear]["sex"]["country,status"][nextSex].Incr(int(nextCountry), int(nextStatus))

	idx.query[prevYear]["status"]["country,status"][prevStatus].Decr(int(prevCountry), int(prevStatus))
	idx.query[nextYear]["status"]["country,status"][acc.Status].Incr(int(nextCountry), int(nextStatus))

	idx.query[prevYear]["status"]["city,sex"][prevStatus].Incr(int(prevCity), int(prevSex))
	idx.query[nextYear]["status"]["city,sex"][nextStatus].Incr(int(nextCity), int(nextSex))

	idx.query[prevYear]["sex"]["country,sex"][prevSex].Incr(int(prevCountry), int(prevSex))
	idx.query[nextYear]["sex"]["country,sex"][nextSex].Incr(int(nextCountry), int(nextSex))

	idx.query[prevYear]["status"]["country,sex"][prevStatus].Incr(int(prevCountry), int(prevSex))
	idx.query[nextYear]["status"]["country,sex"][nextStatus].Incr(int(nextCountry), int(nextSex))
}
