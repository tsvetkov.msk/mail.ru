package store

import (
	"context"
	"hlc/internal/model"
	"hlc/internal/utils"
	"net/url"
	"sort"
	"strings"
	"sync"
)

type idxSname struct {
	access sync.Mutex
	dirty  map[string]bool
	store  map[string][]*model.DBItem
}

func (idx *idxSname) Upload() {
	for i, batch := range accounts {
		for j, acc := range batch {
			if acc.Sname == 0 {
				continue
			}
			sname := wordbook.SName.Conv(acc.Sname)
			if idx.store[sname] == nil {
				idx.store[sname] = make([]*model.DBItem, 0)
			}
			idx.dirty[sname] = true
			idx.store[sname] = append(idx.store[sname], &accounts[i][j])
		}
	}
}

func (idx *idxSname) Insert(acc *model.DBItem) {

	sname := wordbook.SName.Conv(acc.Sname)
	if sname == "" {
		return
	}

	idx.access.Lock()
	defer idx.access.Unlock()

	if idx.store[sname] == nil {
		idx.store[sname] = make([]*model.DBItem, 0)
	}
	idx.dirty[sname] = true
	idx.store[sname] = append(idx.store[sname], acc)
}

func (idx *idxSname) Update(acc *model.DBItem, from, to string) {

	if from == to {
		return
	}

	idx.access.Lock()
	defer idx.access.Unlock()

	if from != "" {
		for i, user := range idx.store[from] {
			if user.ID == acc.ID {
				idx.store[from] = append(idx.store[from][:i], idx.store[from][i+1:]...)
				break
			}
		}
	}

	if idx.store[to] == nil {
		idx.store[to] = make([]*model.DBItem, 0)
	}
	idx.dirty[to] = true
	idx.store[to] = append(idx.store[to], acc)
}

func (idx *idxSname) Construct() {

	idx.access.Lock()
	defer idx.access.Unlock()

	for Sname := range idx.store {
		if idx.dirty[Sname] {
			sort.Slice(idx.store[Sname], func(i, j int) bool {
				return idx.store[Sname][i].ID > idx.store[Sname][j].ID
			})
		}
		idx.dirty[Sname] = false
	}
}

func (idx *idxSname) Equal(ctx context.Context, query url.Values, limit int) ([]map[string]interface{}, error) {

	filters, ok := createFilters(query, "sname_eq")
	if !ok {
		// один из фильтров всегда false
		return emptyList, nil
	}

	if wordbook.SName.Get(query.Get("sname_eq")) == 0 {
		return emptyList, nil
	}

	accs := make([]map[string]interface{}, 0, limit)

	for _, acc := range idx.store[query.Get("sname_eq")] {
		select {
		case <-ctx.Done():
			return accs, ctx.Err()
		default:
		}

		if ok := applyFilters(acc, filters); !ok {
			continue
		}

		accs = append(accs, getResponseItem(acc, query))
		if len(accs) == limit {
			break
		}
	}

	return accs, nil
}

func (idx *idxSname) Starts(ctx context.Context, query url.Values, limit int) ([]map[string]interface{}, error) {

	filters, ok := createFilters(query, "sname_starts")
	if !ok {
		// один из фильтров всегда false
		return emptyList, nil
	}

	batches := [][]*model.DBItem{}
	starts := query.Get("sname_starts")

	for sname, batch := range idx.store {
		if strings.HasPrefix(sname, starts) {
			if len(batch) > 0 {
				batches = append(batches, batch)
			}
		}
	}

	if len(batches) == 0 {
		return emptyList, nil
	}

	accs := make([]map[string]interface{}, 0, limit)

	done := make(chan bool, 0)
	defer close(done)

	for acc := range utils.MergeHeap(done, batches...) {
		select {
		case <-ctx.Done():
			return accs, ctx.Err()
		default:
		}

		if ok := applyFilters(acc, filters); !ok {
			continue
		}

		accs = append(accs, getResponseItem(acc, query))
		if len(accs) == limit {
			break
		}
	}

	return accs, nil
}
