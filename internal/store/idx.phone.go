package store

import (
	"context"
	"hlc/internal/model"
	"net/url"
	"sort"
	"sync"
)

type idxPhone struct {
	access sync.Mutex
	dirty  map[string]bool
	store  map[string][]*model.DBItem
}

func (idx *idxPhone) Upload() {
	for i, batch := range accounts {
		for j, acc := range batch {
			if acc.Phone == "" {
				continue
			}
			phone := acc.Phone[2:5]
			if idx.store[phone] == nil {
				idx.store[phone] = make([]*model.DBItem, 0)
			}
			idx.dirty[phone] = true
			idx.store[phone] = append(idx.store[phone], &accounts[i][j])
		}
	}
}

func (idx *idxPhone) Insert(acc *model.DBItem) {

	idx.access.Lock()
	defer idx.access.Unlock()

	if acc.Phone == "" {
		return
	}

	phone := acc.Phone[2:5]

	if idx.store[phone] == nil {
		idx.store[phone] = make([]*model.DBItem, 0)
	}
	idx.dirty[phone] = true
	idx.store[phone] = append(idx.store[phone], acc)
}

func (idx *idxPhone) Update(acc *model.DBItem, from, to string) {

	if from == to {
		return
	}

	idx.access.Lock()
	defer idx.access.Unlock()

	if from != "" {
		phoneFrom := from[2:5]
		for i, user := range idx.store[phoneFrom] {
			if user.ID == acc.ID {
				idx.store[phoneFrom] = append(idx.store[phoneFrom][:i], idx.store[phoneFrom][i+1:]...)
				break
			}
		}
	}

	phoneTo := to[2:5]
	if idx.store[phoneTo] == nil {
		idx.store[phoneTo] = make([]*model.DBItem, 0)
	}
	idx.dirty[phoneTo] = true
	idx.store[phoneTo] = append(idx.store[phoneTo], acc)
}

func (idx *idxPhone) Construct() {

	idx.access.Lock()
	defer idx.access.Unlock()

	for phone := range idx.store {
		if idx.dirty[phone] {
			sort.Slice(idx.store[phone], func(i, j int) bool {
				return idx.store[phone][i].ID > idx.store[phone][j].ID
			})
		}
		idx.dirty[phone] = false
	}
}

func (idx *idxPhone) Filter(ctx context.Context, query url.Values, limit int) ([]map[string]interface{}, error) {

	filters, ok := createFilters(query, "phone_code")
	if !ok {
		// один из фильтров всегда false
		return emptyList, nil
	}

	accs := make([]map[string]interface{}, 0, limit)

	for _, acc := range idx.store[query.Get("phone_code")] {
		select {
		case <-ctx.Done():
			return accs, ctx.Err()
		default:
		}

		if ok := applyFilters(acc, filters); !ok {
			continue
		}

		accs = append(accs, getResponseItem(acc, query))
		if len(accs) == limit {
			break
		}
	}

	return accs, nil
}
