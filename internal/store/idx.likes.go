package store

import (
	"context"
	"hlc/internal/model"
	"net/url"
	"sort"
	"strconv"
	"strings"
)

type idxLikeContains struct {
}

func (idx *idxLikeContains) Scan(query url.Values) <-chan *model.DBItem {
	ch := make(chan *model.DBItem, 1)
	go func() {

		defer close(ch)

		filters := []filterFn{}

		if birth := query.Get("birth"); birth != "" {
			fn, ok := availableFilters["birth_year"](birth)
			if !ok {
				return
			}
			filters = append(filters, fn)
		}
		if joined := query.Get("joined"); joined != "" {
			fn, ok := availableFilters["joined_year"](joined)
			if !ok {
				return
			}
			filters = append(filters, fn)
		}

		likes, err := strconv.Atoi(query.Get("likes"))
		if err != nil {
			return
		}

		bid := (likes - 1) / batchSize
		pid := (likes - 1) % batchSize

		if bid > len(accounts) {
			return
		}

		for _, id := range accounts[bid][pid].Incoming {
			b := (id - 1) / batchSize
			p := (id - 1) % batchSize

			acc := &accounts[b][p]

			if len(filters) > 0 {
				if ok := applyFilters(acc, filters); !ok {
					continue
				}
			}

			ch <- acc
		}

	}()
	return ch
}

func (idx *idxLikeContains) Filter(ctx context.Context, query url.Values, limit int) ([]map[string]interface{}, error) {

	ids := []int32{}
	for _, sid := range strings.Split(query.Get("likes_contains"), ",") {
		id, err := strconv.Atoi(sid)
		if err != nil {
			return emptyList, ErrIncorrectValue
		}
		ids = append(ids, int32(id))
	}

	filters := []filterFn{}

	if len(ids) > 1 {
		filters = append(filters, likeContains(ids[1:]))
	}

	// формируем фильтры
	if fls, ok := createFilters(query, "likes_contains"); ok {
		filters = append(filters, fls...)
	} else {
		// один из фильтров всегда false
		return emptyList, nil
	}

	accs := make([]map[string]interface{}, 0, limit)

	bid := (ids[0] - 1) / batchSize
	pid := (ids[0] - 1) % batchSize

	incoming := accounts[bid][pid].Incoming
	sort.Slice(incoming, func(i, j int) bool {
		return incoming[i] > incoming[j]
	})

	for _, id := range incoming {

		b := (id - 1) / batchSize
		p := (id - 1) % batchSize

		acc := &accounts[b][p]

		if ok := applyFilters(acc, filters); !ok {
			continue
		}

		accs = append(accs, getResponseItem(acc, query))
		if len(accs) == limit {
			break
		}
	}

	return accs, nil
}

func likeContains(likes []int32) filterFn {
	return func(acc *model.DBItem) bool {
	main:
		for _, like := range likes {
			for _, id := range acc.Outgoing {
				if id == like {
					continue main
				}
			}
			return false
		}
		return true
	}
}
