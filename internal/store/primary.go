package store

import (
	"hlc/internal/model"
	"sync"
)

var (
	phones      = map[string]int32{}
	phonesAcces sync.RWMutex

	emails      = map[uint8]map[string]int32{}
	emailsAcces sync.RWMutex
)

func isAccExists(ids ...int32) error {
	for _, id := range ids {
		if int(id) <= batchSize*storageSize {
			continue
		}
		bid := (id - 1) / batchSize
		pid := (id - 1) % batchSize
		if int(bid) > len(accounts) {
			return ErrUserNotFound
		}
		if accounts[bid][pid].ID == 0 {
			return ErrUserNotFound
		}
	}
	return nil
}

func setEmail(email model.Email, id int32) {

	emailsAcces.Lock()
	defer emailsAcces.Unlock()

	if emails[email.Domain] == nil {
		emails[email.Domain] = make(map[string]int32)
	}
	emails[email.Domain][email.Name] = id
}

func getEmail(s string) (int32, bool) {

	email := wordbook.Email.Set(s)

	emailsAcces.RLock()
	defer emailsAcces.RUnlock()

	if list, ok := emails[email.Domain]; ok {
		if id, ok := list[email.Name]; ok {
			return id, true
		}
	}
	return 0, false
}

func deleteEmail(email model.Email) {
	if emails[email.Domain] == nil {
		return
	}

	emailsAcces.Lock()
	{
		delete(emails[email.Domain], email.Name)
	}
	emailsAcces.Unlock()
}

func setPhone(s string, id int32) {

	if s == "" {
		return
	}

	phonesAcces.Lock()
	{
		phones[s] = id
	}
	phonesAcces.Unlock()
}

func getPhone(s string) (int32, bool) {

	if s == "" {
		return 0, false
	}

	phonesAcces.RLock()
	defer phonesAcces.RUnlock()

	if id, ok := phones[s]; ok {
		return id, true
	}
	return 0, false
}

func deletePhone(s string) {
	if s == "" {
		return
	}
	phonesAcces.Lock()
	{
		delete(phones, s)
	}
	phonesAcces.Unlock()
}
