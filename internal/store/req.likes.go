package store

import "hlc/internal/model"

// InsertLikes -
func InsertLikes(likes model.LikeRequest) error {

	users := []int32{}
	for _, like := range likes.List {
		users = append(users, like.Likee, like.Liker)
	}

	if err := isAccExists(users...); err != nil {
		return err
	}

	go func() {
		for _, like := range likes.List {
			liker := model.NewInt24(like.Liker)
			likee := model.NewInt24(like.Likee)

			batchAccess[liker.Shift].Lock()
			accounts[liker.Shift][liker.Value].Outgoing = append(accounts[liker.Shift][liker.Value].Outgoing, like.Likee)
			batchAccess[liker.Shift].Unlock()

			batchAccess[likee.Shift].Lock()
			accounts[likee.Shift][likee.Value].Incoming = append(accounts[likee.Shift][likee.Value].Incoming, like.Liker)
			batchAccess[likee.Shift].Unlock()
		}
	}()

	return nil
}
