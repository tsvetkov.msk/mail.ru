package store

import (
	"context"
	"hlc/internal/model"
	"hlc/internal/utils"
	"net/url"
	"sort"
	"strings"
	"sync"
)

type idxCity struct {
	access sync.Mutex
	dirty  map[uint16]map[uint8]bool
	store  map[uint16]map[uint8][]*model.DBItem
	stat   map[uint16]map[string]groupCounter
}

func (idx *idxCity) Upload() {
	for i, batch := range accounts {
		for j, acc := range batch {
			city := acc.City
			// if city := acc.City; city > 0 {
			sexStatus := acc.Sex | acc.Status
			if idx.store[city] == nil {
				idx.store[city] = make(map[uint8][]*model.DBItem, 0)
				idx.dirty[city] = make(map[uint8]bool)
				idx.stat[city] = map[string]groupCounter{
					"sex":       &groupSexCounter{store: make(map[uint8]int)},
					"status":    &groupStatusCounter{store: make(map[uint8]int)},
					"interests": &groupInterestsCounter{store: make(map[uint8]int)},
				}
			}
			if idx.store[city][sexStatus] == nil {
				idx.store[city][sexStatus] = make([]*model.DBItem, 0)
				idx.dirty[city][sexStatus] = true
			}
			idx.store[city][sexStatus] = append(idx.store[city][sexStatus], &accounts[i][j])
			idx.stat[city]["sex"].Incr(int(acc.Sex))
			idx.stat[city]["status"].Incr(int(acc.Status))
			for k := range acc.Interests {
				idx.stat[city]["interests"].Incr(int(k))
			}
			// }
		}
	}
}

func (idx *idxCity) Insert(acc *model.DBItem) {

	if acc.City == 0 {
		// return
	}

	idx.access.Lock()
	defer idx.access.Unlock()

	sexStatus := acc.Sex | acc.Status
	if idx.store[acc.City] == nil {
		idx.store[acc.City] = make(map[uint8][]*model.DBItem, 0)
		idx.dirty[acc.City] = make(map[uint8]bool)
		idx.stat[acc.City] = map[string]groupCounter{
			"sex":       &groupSexCounter{store: make(map[uint8]int)},
			"status":    &groupStatusCounter{store: make(map[uint8]int)},
			"interests": &groupInterestsCounter{store: make(map[uint8]int)},
		}
	}
	if idx.store[acc.City][sexStatus] == nil {
		idx.store[acc.City][sexStatus] = make([]*model.DBItem, 0)
		idx.dirty[acc.City][sexStatus] = true
	}
	idx.dirty[acc.City][sexStatus] = true
	idx.store[acc.City][sexStatus] = append(idx.store[acc.City][sexStatus], acc)

	idx.stat[acc.City]["sex"].Incr(int(acc.Sex))
	idx.stat[acc.City]["status"].Incr(int(acc.Status))
	for k := range acc.Interests {
		idx.stat[acc.City]["interests"].Incr(int(k))
	}
}

func (idx *idxCity) Update(acc *model.DBItem, city uint16, sex, status uint8) {

	idx.access.Lock()
	defer idx.access.Unlock()

	// if acc.City > 0 {
	sexStatus := acc.Sex | acc.Status
	for i, user := range idx.store[acc.City][sexStatus] {
		if user.ID == acc.ID {
			idx.store[acc.City][sexStatus] = append(idx.store[acc.City][sexStatus][:i], idx.store[acc.City][sexStatus][i+1:]...)
			break
		}
	}
	idx.stat[acc.City]["sex"].Decr(int(acc.Sex))
	idx.stat[acc.City]["status"].Decr(int(acc.Status))
	// }
	// if city > 0 {
	sexStatus = sex | status
	if idx.store[city] == nil {
		idx.store[city] = make(map[uint8][]*model.DBItem, 0)
		idx.dirty[city] = make(map[uint8]bool)
		idx.stat[city] = map[string]groupCounter{
			"sex":       &groupSexCounter{store: make(map[uint8]int)},
			"status":    &groupStatusCounter{store: make(map[uint8]int)},
			"interests": &groupInterestsCounter{store: make(map[uint8]int)},
		}
	}
	if idx.store[city][sexStatus] == nil {
		idx.store[city][sexStatus] = make([]*model.DBItem, 0)
		idx.dirty[city][sexStatus] = true
	}
	idx.dirty[city][sexStatus] = true
	idx.store[city][sexStatus] = append(idx.store[city][sexStatus], acc)

	idx.stat[city]["sex"].Incr(int(sex))
	idx.stat[city]["status"].Incr(int(status))
	// }
}

func (idx *idxCity) Construct() {

	idx.access.Lock()
	defer idx.access.Unlock()

	for city := range idx.store {
		for sest := range idx.store[city] {
			if idx.dirty[city][sest] {
				sort.Slice(idx.store[city][sest], func(i, j int) bool {
					return idx.store[city][sest][i].ID > idx.store[city][sest][j].ID
				})
			}
			idx.dirty[city][sest] = false
		}
	}
}

func (idx *idxCity) Filter(ctx context.Context, query url.Values, limit int) ([]map[string]interface{}, error) {

	filters, ok := createFilters(query, "sex_eq", "status_eq", "status_neq", "city_eq")
	if !ok {
		// один из фильтров всегда false
		return emptyList, nil
	}

	city := wordbook.City.Get(query.Get("city_eq"))
	if city == 0 || idx.store[city] == nil {
		return emptyList, nil
	}

	sex := wordbook.Sex.Get(query.Get("sex_eq"))
	equal := true
	status := wordbook.Status.Get(query.Get("status_eq"))
	if status == 0 {
		status = wordbook.Status.Get(query.Get("status_neq"))
		equal = false
	}

	batches := [][]*model.DBItem{}
	for sest := range idx.store[city] {
		if len(idx.store[city][sest]) == 0 {
			continue
		}
		if sex == 0 || sex&sest > 0 {
			if status == 0 {
				batches = append(batches, idx.store[city][sest])
				continue
			}
			if (equal && status&sest > 0) || (!equal && status&sest == 0) {
				batches = append(batches, idx.store[city][sest])
			}
		}
	}

	done := make(chan bool, 0)
	defer close(done)

	accs := make([]map[string]interface{}, 0, limit)

	for acc := range utils.MergeHeap(done, batches...) {
		select {
		case <-ctx.Done():
			return accs, ctx.Err()
		default:
		}

		if ok := applyFilters(acc, filters); !ok {
			continue
		}

		accs = append(accs, getResponseItem(acc, query))
		if len(accs) == limit {
			break
		}
	}

	return accs, nil
}

func (idx *idxCity) Any(ctx context.Context, query url.Values, limit int) ([]map[string]interface{}, error) {

	filters, ok := createFilters(query, "sex_eq", "status_eq", "status_neq", "city_any")
	if !ok {
		// один из фильтров всегда false
		return emptyList, nil
	}

	cities := []uint16{}
	for _, cityName := range strings.Split(query.Get("city_any"), ",") {
		city := wordbook.City.Get(cityName)
		if city == 0 || idx.store[city] == nil {
			continue
		}
		cities = append(cities, city)
	}

	sex := wordbook.Sex.Get(query.Get("sex_eq"))
	equal := true
	status := wordbook.Status.Get(query.Get("status_eq"))
	if status == 0 {
		status = wordbook.Status.Get(query.Get("status_neq"))
		equal = false
	}

	batches := [][]*model.DBItem{}
	for _, city := range cities {
		for sest := range idx.store[city] {
			if len(idx.store[city][sest]) == 0 {
				continue
			}
			if sex == 0 || sex&sest > 0 {
				if status == 0 {
					batches = append(batches, idx.store[city][sest])
					continue
				}
				if (equal && status&sest > 0) || (!equal && status&sest == 0) {
					batches = append(batches, idx.store[city][sest])
				}
			}
		}
	}

	done := make(chan bool, 0)
	defer close(done)

	accs := make([]map[string]interface{}, 0, limit)

	for acc := range utils.MergeHeap(done, batches...) {
		select {
		case <-ctx.Done():
			return accs, ctx.Err()
		default:
		}

		if ok := applyFilters(acc, filters); !ok {
			continue
		}

		accs = append(accs, getResponseItem(acc, query))
		if len(accs) == limit {
			break
		}
	}

	return accs, nil
}

func (idx *idxCity) Null(ctx context.Context, query url.Values, limit int) ([]map[string]interface{}, error) {

	filters, ok := createFilters(query, "sex_eq", "status_eq", "status_neq", "city_null")
	if !ok {
		// один из фильтров всегда false
		return emptyList, nil
	}

	sex := wordbook.Sex.Get(query.Get("sex_eq"))
	equal := true
	status := wordbook.Status.Get(query.Get("status_eq"))
	if status == 0 {
		status = wordbook.Status.Get(query.Get("status_neq"))
		equal = false
	}

	batches := [][]*model.DBItem{}
	for sest := range idx.store[0] {
		if len(idx.store[0][sest]) == 0 {
			continue
		}
		if sex == 0 || sex&sest > 0 {
			if status == 0 {
				batches = append(batches, idx.store[0][sest])
				continue
			}
			if (equal && status&sest > 0) || (!equal && status&sest == 0) {
				batches = append(batches, idx.store[0][sest])
			}
		}
	}

	done := make(chan bool, 0)
	defer close(done)

	accs := make([]map[string]interface{}, 0, limit)

	for acc := range utils.MergeHeap(done, batches...) {
		select {
		case <-ctx.Done():
			return accs, ctx.Err()
		default:
		}

		if ok := applyFilters(acc, filters); !ok {
			continue
		}

		accs = append(accs, getResponseItem(acc, query))
		if len(accs) == limit {
			break
		}
	}

	return accs, nil
}

func (idx *idxCity) Stat(query url.Values, key string) (groupCounter, bool) {
	if len(query) > 1 {
		return nil, false
	}
	id := wordbook.City.Get(query.Get("city"))
	if id == 0 {
		return nil, false
	}

	if idx.stat[id] == nil {
		return nil, false
	}

	groups, ok := idx.stat[id][key]
	if ok {
		return groups, true
	}
	return nil, false
}

func (idx *idxCity) Scan(query url.Values) <-chan *model.DBItem {
	ch := make(chan *model.DBItem, 1)
	go func() {

		defer close(ch)

		id := wordbook.City.Get(query.Get("city"))
		if id == 0 {
			return
		}

		filters := []filterFn{}

		if birth := query.Get("birth"); birth != "" {
			fn, ok := availableFilters["birth_year"](birth)
			if !ok {
				return
			}
			filters = append(filters, fn)
		}
		if joined := query.Get("joined"); joined != "" {
			fn, ok := availableFilters["joined_year"](joined)
			if !ok {
				return
			}
			filters = append(filters, fn)
		}

		batches := [][]*model.DBItem{}
		for _, batch := range idx.store[id] {
			if len(batch) == 0 {
				continue
			}
			batches = append(batches, batch)
		}

		done := make(chan bool, 0)
		defer close(done)

		for acc := range utils.MergeHeap(done, batches...) {
			if len(filters) > 0 {
				if ok := applyFilters(acc, filters); !ok {
					continue
				}
			}
			ch <- acc
		}
	}()
	return ch
}
