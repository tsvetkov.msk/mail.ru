package store

import (
	"fmt"
	"hlc/internal/model"
	"runtime"
	"runtime/debug"
	"time"
)

// Init -
func Init(ts int64) {

	timestamp = int32(ts)

	for i, batch := range accounts {
		for _, acc := range batch {
			for _, like := range acc.Outgoing {
				bid := (like - 1) / batchSize
				pid := (like - 1) % batchSize
				accounts[bid][pid].Incoming = append(accounts[bid][pid].Incoming, acc.ID)
			}
			setEmail(acc.Email, acc.ID)
			setPhone(acc.Phone, acc.ID)
		}

		runtime.GC()
		debug.FreeOSMemory()
		fmt.Printf("%d..", i)
	}

	runtime.GC()
	debug.FreeOSMemory()
	printMemUsage("LikeContains ready")

	likeContainsCursor = &idxLikeContains{}

	for i, batch := range accounts {
		for j, acc := range batch {
			income := make([]int32, 0, len(acc.Incoming)+4)
			for _, item := range acc.Incoming {
				income = append(income, item)
			}
			accounts[i][j].Incoming = income

			outgoing := make([]int32, 0, len(acc.Outgoing)+4)
			for _, item := range acc.Outgoing {
				outgoing = append(outgoing, item)
			}
			accounts[i][j].Outgoing = outgoing
		}
		runtime.GC()
		debug.FreeOSMemory()
		fmt.Printf("%d..", i)
	}

	runtime.GC()
	debug.FreeOSMemory()
	printMemUsage("Cleaned")

	sexStatusCursor = &idxSexStatus{
		dirty: []map[uint8]bool{},
		store: []map[uint8][]*model.DBItem{},
	}
	sexStatusCursor.Upload()
	runtime.GC()
	debug.FreeOSMemory()
	printMemUsage("SexStatus ready")

	cityCursor = &idxCity{
		dirty: make(map[uint16]map[uint8]bool),
		store: make(map[uint16]map[uint8][]*model.DBItem),
		stat:  make(map[uint16]map[string]groupCounter),
	}
	cityCursor.Upload()
	runtime.GC()
	debug.FreeOSMemory()
	printMemUsage("CityCursor ready")

	countryCursor = &idxCountry{
		dirty: make(map[uint8]map[uint8]bool),
		store: make(map[uint8]map[uint8][]*model.DBItem),
		stat:  make(map[uint8]map[string]groupCounter),
	}
	countryCursor.Upload()
	runtime.GC()
	debug.FreeOSMemory()
	printMemUsage("CountryCursor ready")

	interestsCursor = &idxInterests{
		dirty: make(map[uint8]bool),
		store: make(map[uint8][]*model.DBItem),
		stat:  map[uint8]map[string]groupCounter{},
	}
	interestsCursor.Upload()
	runtime.GC()
	debug.FreeOSMemory()
	printMemUsage("InterestsCursor ready")

	birthCursor = &idxBirth{
		dirty: make(map[string]bool),
		store: make(map[string][]*model.DBItem),
		stat:  make(map[string]map[string]groupCounter),
		query: make(map[string]map[string]map[string]map[uint8]groupCounter),
	}
	birthCursor.Upload()
	runtime.GC()
	debug.FreeOSMemory()
	printMemUsage("BirthCursor ready")

	phoneCursor = &idxPhone{
		dirty: make(map[string]bool),
		store: make(map[string][]*model.DBItem),
	}
	phoneCursor.Upload()
	runtime.GC()
	debug.FreeOSMemory()
	printMemUsage("PhoneCursor ready")

	fnameCursor = &idxFname{
		dirty: make(map[uint8]bool),
		store: make(map[uint8]map[uint8][]*model.DBItem),
		stat:  map[uint8]map[uint8]int{},
	}
	fnameCursor.Upload()
	runtime.GC()
	debug.FreeOSMemory()
	printMemUsage("FnameCursor ready")

	snameCursor = &idxSname{
		dirty: make(map[string]bool),
		store: make(map[string][]*model.DBItem),
	}
	snameCursor.Upload()
	runtime.GC()
	debug.FreeOSMemory()
	printMemUsage("SnameCursor ready")

	joinedCursor = &idxJoined{
		stat:  make(map[string]map[string]groupCounter),
		query: make(map[string]map[string]map[string]map[uint8]groupCounter),
	}
	joinedCursor.Upload()
	runtime.GC()
	debug.FreeOSMemory()
	printMemUsage("JoinedCursor ready")

	go updateIndexes()
}

func updateIndexes() {
	for {
		select {
		case acc := <-updater:
			if acc == nil {
				continue
			}
			isFirstPhase = false
			go sexStatusCursor.Insert(acc)
			go cityCursor.Insert(acc)
			go countryCursor.Insert(acc)
			go interestsCursor.Insert(acc)
			go birthCursor.Insert(acc)
			go phoneCursor.Insert(acc)
			go fnameCursor.Insert(acc)
			go snameCursor.Insert(acc)
			go joinedCursor.Insert(acc)
		case <-time.After(time.Second * 3):
			go sexStatusCursor.Construct()
			go cityCursor.Construct()
			go countryCursor.Construct()
			go interestsCursor.Construct()
			go birthCursor.Construct()
			go phoneCursor.Construct()
			go fnameCursor.Construct()
			go snameCursor.Construct()
			debug.FreeOSMemory()
		}
	}
}
