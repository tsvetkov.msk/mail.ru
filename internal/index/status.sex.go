package index

import (
	"hlc/internal/model"
	"net/url"
)

// IdxStatusSex -
type IdxStatusSex struct {
	store map[uint8]*IdxSex
}

// Insert -
func (idx *IdxStatusSex) Insert(acc *model.DBItem) {

}

// Remove -
func (idx *IdxStatusSex) Remove(acc *model.DBItem) {

}

// Range -
func (idx *IdxStatusSex) Range(done chan bool, query url.Values) <-chan *model.DBItem {
	return nil
}
