package index

import (
	"hlc/internal/model"
	"hlc/internal/utils"
	"net/url"
	"sync"
)

var (
	gender = map[string]uint8{"m": 1, "f": 2}
)

// IdxSex -
type IdxSex struct {
	access []sync.Mutex
	store  []map[uint8][]*model.DBItem
}

// Insert -
func (idx *IdxSex) Insert(acc *model.DBItem) {
	batch := int(acc.ID-1) / 10000
	if len(idx.store) <= batch {
		idx.store = append(idx.store, map[uint8][]*model.DBItem{
			1: []*model.DBItem{},
			2: []*model.DBItem{},
		})
		idx.access = append(idx.access, sync.Mutex{})
	}
	idx.access[batch].Lock()
	defer idx.access[batch].Unlock()

	if len(idx.store[batch][acc.Sex]) == 0 {
		idx.store[batch][acc.Sex] = append(idx.store[batch][acc.Sex], acc)
	}

	for i, ex := range idx.store[batch][acc.Sex] {
		if acc.ID > ex.ID {
			idx.store[batch][acc.Sex] = appendAfter(idx.store[batch][acc.Sex], i-1, acc)
		}
	}
}

// Remove -
func (idx *IdxSex) Remove(acc *model.DBItem) {
	batch := int(acc.ID-1) / 10000
	for i, ex := range idx.store[batch][acc.Sex] {
		if acc.ID > ex.ID {
			idx.store[batch][acc.Sex] = append(idx.store[batch][acc.Sex][:i], idx.store[batch][acc.Sex][i+1:]...)
		}
	}
}

// Range -
func (idx *IdxSex) Range(done chan bool, query url.Values) <-chan *model.DBItem {
	ch := make(chan *model.DBItem, 1)
	go func() {

		defer close(ch)

		sex := gender[query.Get("sex_eq")]
		for _, batch := range idx.store {
			batches := [][]*model.DBItem{}
			for k, v := range batch {
				if k&sex == sex {
					batches = append(batches, v)
				}
			}
			for acc := range utils.MergeHeap(done, batches...) {
				ch <- acc
			}
		}
	}()
	return ch
}

func appendAfter(a []*model.DBItem, idx int, el *model.DBItem) []*model.DBItem {

	result := make([]*model.DBItem, len(a)+1)
	copy(result[:idx], a[:idx])
	result[idx] = el
	copy(result[idx+1:], a[idx:])

	return result
}
