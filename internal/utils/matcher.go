package utils

import (
	"net/url"
	"sort"
	"strings"
)

/*

fname_null_0 	1182380
country_null_0 	1053210
sname_null_0  	957762
premium_null_1 	895881
premium_now_0 	895881
city_null_0 	793891
phone_null_1 	766711
phone_null_0 	533289
city_null_1 	506109
premium_null_0 	404119
sname_null_1  	342238
country_null_1 	246790
premium_now_1 	151465
fname_null_1 	117620

*/

var (
	priorityQuery = map[string]int{

		"likes_contains": 191, //
		"sname_eq":       100, // 	1023
		"sname_starts":   90,  //
		"phone_code":     98,  // 	5519
		"fname_eq":       97,  // 	11827
		"city_eq":        96,  //	14935
		"country_eq":     97,  //	36490
		"email_domain":   95,  //	101133
		"fname_null_1":   94,  // 	117620

		"birth_year":     92, //	186489
		"country_null_1": 92, // 	246790
		"sname_null_1":   91, // 	342238

		"premium_null_0": 89, // 	404119
		"status_eq":      88, //	435000
		"city_null_1":    87, // 	506109
		"phone_null_0":   86, // 	533289
		"sex_eq":         85, //	650000
		"phone_null_1":   84, // 	766711
		"city_null_0":    83, // 	793891
		"status_neq":     82, //	870000
		"premium_null_1": 81, //	895881
		"sname_null_0":   80, //	957762
		"country_null_0": 79, //	1053210
		"fname_null_0":   78, //	1182380
		"premium_now":    77, //	151465

		"interests_contains": 77, //

		"birth_lt":      76, //
		"birth_gt":      75, //
		"email_lt":      74, //
		"email_gt":      73, //
		"interests_any": 70, //
		"city_any":      69, //
		"fname_any":     68, //

	}
)

// MatchURL -
func MatchURL(query url.Values, exclude ...string) []string {

	filters := make([]string, 0, len(query))

main:
	for k, v := range query {
		for _, ex := range exclude {
			if k == ex {
				continue main
			}
		}
		if strings.HasSuffix(k, "_null") {
			k += "_" + v[0]
		}
		filters = append(filters, k)
	}

	switch len(filters) {
	case 0, 1:
		return filters
	}

	sort.Slice(filters, func(i, j int) bool {
		return priorityQuery[filters[i]] > priorityQuery[filters[j]]
	})

	return filters
}
