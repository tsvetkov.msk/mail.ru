package utils

import "hlc/internal/model"

// MergeHeap -
func MergeHeap(done <-chan bool, slices ...[]*model.DBItem) <-chan *model.DBItem {
	ch := make(chan *model.DBItem, 1)

	go func() {

		defer close(ch)

		idxes := make([]int, len(slices))
		lens := make([]int, len(slices))
		values := make([]*model.DBItem, len(slices))

		hasValues := func() bool {
			for i := range slices {
				if lens[i] > idxes[i] {
					return true
				}
			}
			return false
		}

		for i, s := range slices {
			lens[i] = len(s)
			values[i] = s[0]
		}

		for hasValues() {

			var idx int
			var value *model.DBItem

			for i := range slices {
				if values[i] == nil {
					continue
				}
				if value == nil {
					idx = i
					value = slices[i][idxes[i]]
					continue
				}
				if values[i].ID > value.ID {
					idx = i
					value = values[i]
				}
			}

			select {
			case <-done:
				return
			case ch <- value:
			}

			idxes[idx]++

			if lens[idx] == idxes[idx] {
				values[idx] = nil
			} else {
				values[idx] = slices[idx][idxes[idx]]
			}
		}

	}()

	return ch
}
