package model

import (
	"math/rand"
	"testing"
	"time"
)

func Test_Int24(t *testing.T) {
	for i := 0; i < 1000000; i++ {
		rand.Seed(time.Now().UnixNano())
		n := rand.Int31n(1350000) + 1
		bid := (n - 1) / 10000
		pid := (n - 1) % 10000
		int24 := Int24{Shift: uint8(bid), Value: uint16(pid)}

		if int24.Int32() != n {
			t.Fatalf("Incorrect value; Got %d; Want %d", int24.Int32(), n)
		}
	}
}
