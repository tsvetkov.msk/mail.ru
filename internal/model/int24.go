package model

import json "encoding/json"

// Int24 -
type Int24 struct {
	Shift uint8
	Value uint16
}

// Int32 -
func (n Int24) Int32() int32 {
	return int32(n.Shift)*10000 + int32(n.Value) + 1
}

// Equal -
func (n Int24) Equal(m Int24) bool {
	return n.Shift == m.Shift && n.Value == m.Value
}

// More -
func (n Int24) More(m Int24) bool {
	if n.Shift > m.Shift {
		return true
	}
	return n.Value > m.Value
}

// Copy -
func (n Int24) Copy() Int24 {
	return Int24{
		Shift: uint8(n.Shift),
		Value: uint16(n.Value),
	}
}

// NewInt24 -
func NewInt24(base int32) Int24 {
	return Int24{
		Shift: uint8((base - 1) / 10000),
		Value: uint16((base - 1) % 10000),
	}
}

// MarshalJSON -
func (n *Int24) MarshalJSON() ([]byte, error) {
	return json.Marshal(n.Int32())
}
