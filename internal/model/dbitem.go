package model

// DBItem -
type DBItem struct {
	ID        int32
	Premium   Premium
	Sex       uint8
	Status    uint8
	Fname     uint8
	Sname     uint16
	Country   uint8
	City      uint16
	Email     Email
	Phone     string
	Birth     int32
	Joined    int32
	Incoming  []int32
	Outgoing  []int32
	Interests map[uint8]struct{}
}

// Premium -
type Premium struct {
	Shift  uint8
	Finish int32
}

// Email -
type Email struct {
	Domain uint8
	Name   string
}
