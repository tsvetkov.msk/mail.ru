package model

// UpdAcc -
type UpdAcc struct {
	ID        int32    `json:"-"`
	Birth     int32    `json:"birth"`
	Email     string   `json:"email"`
	FName     string   `json:"fname"`
	SName     string   `json:"sname"`
	Phone     string   `json:"phone"`
	Gender    string   `json:"sex"`
	Country   string   `json:"country"`
	City      string   `json:"city"`
	Joined    int32    `json:"joined"`
	Status    string   `json:"status"`
	Interests []string `json:"interests"`

	Premium struct {
		Start  int32 `json:"start,required"`
		Finish int32 `json:"finish,required"`
	} `json:"premium"`

	Likes []LikesItem `json:"likes"`
}

// LikeRequest -
type LikeRequest struct {
	List []struct {
		DT    int32 `json:"ts,required"`
		Likee int32 `json:"likee,required"`
		Liker int32 `json:"liker,required"`
	} `json:"likes,required"`
}
