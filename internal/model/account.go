package model

// Account -
type Account struct {
	ID        int32    `json:"id,required"`
	Birth     int32    `json:"birth,required"`
	Email     string   `json:"email,required"`
	FName     string   `json:"fname"`
	SName     string   `json:"sname"`
	Phone     string   `json:"phone"`
	Gender    string   `json:"sex,required"`
	Country   string   `json:"country"`
	City      string   `json:"city"`
	Joined    int32    `json:"joined,required"`
	Status    string   `json:"status,required"`
	Interests []string `json:"interests"`

	Premium struct {
		Start  int32 `json:"start,required"`
		Finish int32 `json:"finish,required"`
	} `json:"premium"`

	Likes []LikesItem `json:"likes"`
}

// LikesItem -
type LikesItem struct {
	ID int32 `json:"id,required"`
	DT int32 `json:"ts,required"`
}
