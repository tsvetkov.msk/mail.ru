package dict

import (
	"hlc/internal/model"
	"strings"
)

type dictEmail struct {
	Current uint8
	Direct  map[string]uint8
	Reverce map[uint8]string
}

func (d *dictEmail) Get(domain string) uint8 {
	return d.Direct[domain]
}

func (d *dictEmail) Set(s string) model.Email {

	email := strings.Split(s, "@")

	if n, ok := d.Direct[email[1]]; ok {
		return model.Email{Domain: n, Name: email[0]}
	}

	d.Current++

	d.Direct[email[1]] = d.Current
	d.Reverce[d.Current] = email[1]

	return model.Email{Domain: d.Current, Name: email[0]}
}

func (d *dictEmail) Conv(email model.Email) string {
	return email.Name + "@" + d.Reverce[email.Domain]
}
