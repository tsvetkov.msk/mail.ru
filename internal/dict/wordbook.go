package dict

// Wordbook -
type Wordbook struct {
	SName     dictStrUint16
	City      dictStrUint16
	Status    dictStrUint8
	Sex       dictStrUint8
	FName     dictStrUint8
	Country   dictStrUint8
	Interests dictStrUint8
	Email     dictEmail
	Premium   dictPremium
}

// New -
func New() *Wordbook {
	return &Wordbook{
		City: dictStrUint16{
			Direct:  make(map[string]uint16),
			Reverce: make(map[uint16]string),
		},
		SName: dictStrUint16{
			Direct:  make(map[string]uint16),
			Reverce: make(map[uint16]string),
		},
		Status: dictStrUint8{
			Direct: map[string]uint8{
				"свободны":   4,
				"заняты":     8,
				"всё сложно": 16,
			},
			Reverce: map[uint8]string{
				4:  "свободны",
				8:  "заняты",
				16: "всё сложно",
			},
			Current: 16,
		},
		Sex: dictStrUint8{
			Direct: map[string]uint8{
				"m": 1,
				"f": 2,
			},
			Reverce: map[uint8]string{
				1: "m",
				2: "f",
			},
			Current: 2,
		},
		FName: dictStrUint8{
			Direct:  make(map[string]uint8),
			Reverce: make(map[uint8]string),
		},
		Country: dictStrUint8{
			Direct:  make(map[string]uint8),
			Reverce: make(map[uint8]string),
		},
		Email: dictEmail{
			Direct:  make(map[string]uint8),
			Reverce: make(map[uint8]string),
		},
		Interests: dictStrUint8{
			Direct:  make(map[string]uint8),
			Reverce: make(map[uint8]string),
		},
		Premium: dictPremium{
			Direct:  make(map[int32]uint8),
			Reverce: make(map[uint8]int32),
		},
	}
}

type dictStrUint8 struct {
	Current uint8
	Direct  map[string]uint8
	Reverce map[uint8]string
}

func (d *dictStrUint8) Get(v string) uint8 {

	if v == "" {
		return 0
	}

	return d.Direct[v]
}

func (d *dictStrUint8) Neq(s string) uint8 {

	res := uint8(0)
	for k, v := range d.Direct {
		if k != s {
			res |= v
		}
	}

	return res
}

func (d *dictStrUint8) Set(s string) uint8 {

	if s == "" {
		return 0
	}

	if n, ok := d.Direct[s]; ok {
		return n
	}

	d.Current++

	d.Direct[s] = d.Current
	d.Reverce[d.Current] = s

	return d.Current
}
func (d *dictStrUint8) Conv(n uint8) string {
	if n == 0 || n > d.Current {
		return ""
	}
	return d.Reverce[n]
}

type dictStrUint16 struct {
	Current uint16
	Direct  map[string]uint16
	Reverce map[uint16]string
}

func (d *dictStrUint16) Get(v string) uint16 {
	if v == "" {
		return 0
	}
	return d.Direct[v]
}

func (d *dictStrUint16) Set(s string) uint16 {

	if s == "" {
		return 0
	}

	if n, ok := d.Direct[s]; ok {
		return n
	}

	d.Current++

	d.Direct[s] = d.Current
	d.Reverce[d.Current] = s

	return d.Current
}

func (d *dictStrUint16) Conv(n uint16) string {
	if n == 0 || n > d.Current {
		return ""
	}
	return d.Reverce[n]
}
