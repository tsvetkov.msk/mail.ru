package dict

import (
	"hlc/internal/model"
)

type dictPremium struct {
	Current uint8
	Direct  map[int32]uint8
	Reverce map[uint8]int32
}

func (dp *dictPremium) Set(start, finish int32) model.Premium {
	if start == 0 || finish == 0 {
		return model.Premium{}
	}
	shift := finish - start
	if v, ok := dp.Direct[shift]; ok {
		return model.Premium{Finish: finish, Shift: v}
	}

	dp.Current++

	dp.Direct[shift] = dp.Current
	dp.Reverce[dp.Current] = shift

	return model.Premium{Finish: finish, Shift: dp.Current}
}

func (dp *dictPremium) Get(premium model.Premium) map[string]int32 {
	if premium.Shift == 0 {
		return nil
	}
	return map[string]int32{
		"finish": premium.Finish,
		"start":  premium.Finish - dp.Reverce[premium.Shift],
	}
}
