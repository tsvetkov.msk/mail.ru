package stat

import (
	"hlc/internal/model"
	"sync"
	"time"
)

type cityCounter struct {
	access sync.Mutex

	BirthSex      map[int]map[uint8]map[uint16]int
	BirthStatus   map[int]map[uint8]map[uint16]int
	BirthInterest map[int]map[uint8]map[uint16]int

	JoinedSex      map[int]map[uint8]map[uint16]int
	JoinedStatus   map[int]map[uint8]map[uint16]int
	JoinedInterest map[int]map[uint8]map[uint16]int
}

func (c *cityCounter) Incr(acc *model.DBItem) {

	bYear := time.Unix(int64(acc.Birth), 0).Year()
	jYear := time.Unix(int64(acc.Joined), 0).Year()

	c.access.Lock()
	defer c.access.Unlock()

	if c.BirthSex[bYear] == nil {
		c.BirthSex[bYear] = make(map[uint8]map[uint16]int)
		c.BirthStatus[bYear] = make(map[uint8]map[uint16]int)
		c.BirthInterest[bYear] = make(map[uint8]map[uint16]int)
	}

	if c.JoinedSex[jYear] == nil {
		c.JoinedSex[jYear] = make(map[uint8]map[uint16]int)
		c.JoinedStatus[jYear] = make(map[uint8]map[uint16]int)
		c.JoinedInterest[jYear] = make(map[uint8]map[uint16]int)
	}

	for k := range acc.Interests {
		if c.BirthInterest[bYear][k] == nil {
			c.BirthInterest[bYear][k] = make(map[uint16]int)
		}
		c.BirthInterest[bYear][k][acc.City]++

		if c.JoinedInterest[jYear][k] == nil {
			c.JoinedInterest[jYear][k] = make(map[uint16]int)
		}
		c.JoinedInterest[jYear][k][acc.City]++
	}
}

func (c *cityCounter) Decr(acc *model.DBItem) {

	bYear := time.Unix(int64(acc.Birth), 0).Year()
	jYear := time.Unix(int64(acc.Joined), 0).Year()

	c.access.Lock()
	defer c.access.Unlock()

	for k := range acc.Interests {
		c.BirthInterest[bYear][k][acc.City]--
		c.JoinedInterest[jYear][k][acc.City]--
	}
}
