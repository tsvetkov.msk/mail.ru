FROM alpine:3.7

COPY ./bin/highloadcup /opt/highloadcup
COPY ./bin/entrypoint.sh /opt/entrypoint.sh

RUN chmod +x /opt/entrypoint.sh
RUN chmod +x /opt/highloadcup

EXPOSE 80

ENTRYPOINT ["sh", "-c", "/opt/entrypoint.sh"]