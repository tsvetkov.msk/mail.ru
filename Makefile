IMAGE?=stor.highloadcup.ru/accounts/crane_climber
TEST_DATA?=${PWD}/data/220119/data
RAITING_DATA?=${PWD}/data/rating/data

codogen:
	~/go/bin/easyjson -all -omit_empty -disallow_unknown_fields ./internal/model/account.go

build:codogen
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o bin/highloadcup ./cmd/fasthttp/

image:build
	docker build -t ${IMAGE} .
	rm bin/highloadcup
	docker system prune -f

run:
	docker run -it -m 128m --rm -p 8080:80 -v ${TEST_DATA}:/tmp/data ${IMAGE}

rating:
	docker run -it -m 2048m --rm -p 8080:80 -v ${RAITING_DATA}:/tmp/data ${IMAGE}

release:
	docker push ${IMAGE}

fight:
	echo "Phase I"
	highloadcup_tester -addr http://localhost:8080 -hlcupdocs data/220119 -utf8 -phase 1 -filter "/accounts/group/" > tank.1.log
	echo "Phase II"
	sleep 5
	highloadcup_tester -addr http://localhost:8080 -hlcupdocs data/220119 -utf8 -phase 2 > tank.2.log
	echo "Phase III"
	sleep 5
	highloadcup_tester -addr http://localhost:8080 -hlcupdocs data/220119 -utf8 -phase 3 -filter "/accounts/group/" > tank.3.log

rfight:
	echo "Phase I"
	highloadcup_tester -addr http://localhost:8080 -hlcupdocs data/rating -utf8 -phase 1 -concurrent 10 -filter "/accounts/filter/" > tank.1.log
	echo "Phase II"
	sleep 5
	highloadcup_tester -addr http://localhost:8080 -hlcupdocs data/rating -utf8 -phase 2 > tank.2.log
	echo "Phase III"
	sleep 5
	highloadcup_tester -addr http://localhost:8080 -hlcupdocs data/rating -utf8 -phase 3 -concurrent 20 -filter "/accounts/filter/" > tank.3.log
