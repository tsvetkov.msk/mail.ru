package main

import (
	"encoding/json"
	"fmt"
	"hlc/internal/store"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/valyala/fasthttp"
)

var (
	availableSuggestQuery = map[string]bool{
		"country": true,
		"city":    true,
	}
)

func suggestAccount(uid string) fasthttp.RequestHandler {

	return func(ctx *fasthttp.RequestCtx) {

		start := time.Now()

		id, err := strconv.Atoi(uid)
		if err != nil {
			noContentHandler(ctx, http.StatusNotFound)
			return
		}
		limit, err := ctx.QueryArgs().GetUint("limit")
		if err != nil {
			noContentHandler(ctx, http.StatusBadRequest)
			return
		}

		_, _ = limit, id

		rQuery, err := url.ParseQuery(ctx.QueryArgs().String())
		if err != nil {
			noContentHandler(ctx, http.StatusBadRequest)
			return
		}

		rQuery.Del("query_id")
		rQuery.Del("limit")

		for key, value := range rQuery {
			if _, ok := availableSuggestQuery[key]; !ok {
				noContentHandler(ctx, http.StatusBadRequest)
				return
			}
			if value[0] == "" {
				noContentHandler(ctx, http.StatusBadRequest)
				return
			}
		}

		defer func() {
			if since := time.Since(start); since > time.Millisecond*500 {
				fmt.Printf("Suggest %.5f\n", since.Seconds())
			}
		}()

		accs, err := store.Suggest(int32(id), limit)
		if err != nil {
			noContentHandler(ctx, http.StatusNotFound)
			return
		}

		ctx.SetContentType("application/json")

		if len(accs) == 0 {
			ctx.Write(emptyAccounts)
			return
		}

		json.NewEncoder(ctx).Encode(map[string]interface{}{
			"accounts": accs,
		})
	}
}
