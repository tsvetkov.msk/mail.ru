package main

import (
	"flag"
	"fmt"
	"log"
	"runtime/debug"
	"time"

	"hlc/internal/store"

	"github.com/valyala/fasthttp"
)

var (
	timestamp = flag.Int64("ts", 1548167049, "")
	addr      = flag.String("addr", ":8080", "")
	mode      = flag.Int("mode", 0, "")
	storePath = flag.String("store", "/Users/andrey/PROJECTS/HLC/data/220119/data", "")
)

func init() {
	flag.Parse()
}

func main() {

	if err := store.Open(*storePath); err != nil {
		log.Fatalf("Can`t open DB: %s", err)
	}

	store.Init(*timestamp)

	go func() {
		for {
			time.Sleep(time.Second * 5)
			debug.FreeOSMemory()
		}
	}()

	fmt.Println("ListenAndServe", *addr)

	if err := fasthttp.ListenAndServe(*addr, createHTTPHandler()); err != nil {
		log.Fatalf("Error in ListenAndServe: %s", err)
	}
}
