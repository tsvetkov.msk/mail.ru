package main

import (
	"context"
	"encoding/json"
	"fmt"
	"hlc/internal/store"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/valyala/fasthttp"
)

var (
	availableGroupQuery = map[string]bool{
		"keys":      true,
		"joined":    true,
		"order":     true,
		"sex":       true,
		"email":     true,
		"status":    true,
		"fname":     true,
		"sname":     true,
		"phone":     true,
		"country":   true,
		"city":      true,
		"birth":     true,
		"interests": true,
		"likes":     true,
		"premium":   true,
	}
	availableGroupKeys = map[string]bool{
		"sex":       true,
		"status":    true,
		"interests": true,
		"country":   true,
		"city":      true,
	}
)

func groupAccount(ctx *fasthttp.RequestCtx) {

	start := time.Now()

	limit, err := ctx.QueryArgs().GetUint("limit")
	if err != nil || limit > 50 {
		noContentHandler(ctx, http.StatusBadRequest)
		return
	}

	rQuery, err := url.ParseQuery(ctx.QueryArgs().String())
	if err != nil {
		noContentHandler(ctx, http.StatusBadRequest)
		return
	}

	rQuery.Del("query_id")
	rQuery.Del("limit")

	for key, value := range rQuery {
		if value[0] != "" {
			if _, ok := availableGroupQuery[key]; ok {
				continue
			}
		}
		noContentHandler(ctx, http.StatusBadRequest)
		return
	}

	keys := rQuery.Get("keys")
	for _, v := range strings.Split(keys, ",") {
		if ok := availableGroupKeys[v]; !ok {
			noContentHandler(ctx, http.StatusBadRequest)
			return
		}
	}

	defer func() {
		if since := time.Since(start); since > time.Millisecond*100 {
			fmt.Printf("GLR %.4f\t%s\t%v\n", since.Seconds(), keys, rQuery)
		}
	}()

	groups, err := store.Group(rQuery, limit)
	if err != nil {
		if err != context.DeadlineExceeded {
			noContentHandler(ctx, http.StatusBadRequest)
			return
		}
	}

	ctx.SetContentType("application/json")

	if len(groups) == 0 {
		ctx.Write(emptyGroups)
		return
	}

	json.NewEncoder(ctx).Encode(map[string]interface{}{
		"groups": groups,
	})
}
