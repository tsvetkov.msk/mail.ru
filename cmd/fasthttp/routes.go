package main

import (
	"encoding/json"
	"net/http"
	"regexp"

	"hlc/internal/store"

	"github.com/valyala/fasthttp"
)

var (
	emptyGroups   = []byte(`{"groups":[]}`)
	emptyAccounts = []byte(`{"accounts":[]}`)
)

func createHTTPHandler() fasthttp.RequestHandler {

	re := regexp.MustCompile("/accounts/([0-9]+)/(recommend/|suggest/)?")

	return func(ctx *fasthttp.RequestCtx) {

		handler := notFoundHandler
		urlPath := string(ctx.Path())

		switch urlPath {
		case "/accounts/filter/":
			handler = filterAccounts
		case "/accounts/group/":
			handler = groupAccount
		case "/accounts/new/":
			handler = createAccount
		case "/accounts/likes/":
			handler = createLikes
		case "/accounts/info/":
			handler = getAccInfo
		default:

			paths := re.FindStringSubmatch(urlPath)
			if len(paths) != 3 {
				break
			}

			switch paths[2] {
			case "recommend/":
				handler = recomendAccount(paths[1])
			case "suggest/":
				handler = suggestAccount(paths[1])
			default:
				handler = updateAccount(paths[1])
			}
		}

		handler(ctx)
	}
}

func getAccInfo(ctx *fasthttp.RequestCtx) {
	id, err := ctx.QueryArgs().GetUint("id")
	if err != nil {
		noContentHandler(ctx, http.StatusBadRequest)
		return
	}

	json.NewEncoder(ctx).Encode(store.AccInfo(int32(id)))
}

func noContentHandler(ctx *fasthttp.RequestCtx, code int) {
	if code == 201 || code == 202 {
		ctx.WriteString("{}")
		ctx.SetContentType("application/json")
	}
	ctx.Response.SetStatusCode(code)
}

func notFoundHandler(ctx *fasthttp.RequestCtx) {
	ctx.Response.SetStatusCode(404)
}
