package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"hlc/internal/store"

	"github.com/valyala/fasthttp"
)

var (
	availableFilterQuery = map[string]bool{
		"sex_eq":         true,
		"likes_contains": true,
		"email_domain":   true, "email_lt": true, "email_gt": true,
		"status_eq": true, "status_neq": true,
		"fname_eq": true, "fname_any": true, "fname_null": true,
		"sname_eq": true, "sname_starts": true, "sname_null": true,
		"phone_code": true, "phone_null": true,
		"country_eq": true, "country_null": true,
		"city_eq": true, "city_any": true, "city_null": true,
		"birth_lt": true, "birth_gt": true, "birth_year": true,
		"interests_any": true, "interests_contains": true,
		"premium_now": true, "premium_null": true,
	}
)

func filterAccounts(ctx *fasthttp.RequestCtx) {

	start := time.Now()

	limit, err := ctx.QueryArgs().GetUint("limit")
	if err != nil {
		noContentHandler(ctx, http.StatusBadRequest)
		return
	}

	rQuery, err := url.ParseQuery(ctx.QueryArgs().String())
	if err != nil {
		noContentHandler(ctx, http.StatusBadRequest)
		return
	}

	rQuery.Del("query_id")
	rQuery.Del("limit")

	for key, value := range rQuery {
		if value[0] != "" {
			if _, ok := availableFilterQuery[key]; ok {
				continue
			}
		}
		noContentHandler(ctx, http.StatusBadRequest)
		return
	}

	defer func() {
		if since := time.Since(start); since > time.Millisecond*100 {
			fmt.Printf("FLR %.4f\t%v\n", since.Seconds(), rQuery)
		}
	}()

	rctx, cancel := context.WithTimeout(ctx, time.Millisecond*2000)
	defer cancel()

	accs, err := store.Filter(rctx, rQuery, limit)
	if err != nil {
		if err != context.DeadlineExceeded {
			noContentHandler(ctx, http.StatusBadRequest)
			return
		}
	}

	ctx.SetContentType("application/json")

	if len(accs) == 0 {
		ctx.Write(emptyAccounts)
		return
	}

	json.NewEncoder(ctx).Encode(map[string]interface{}{
		"accounts": accs,
	})
}
