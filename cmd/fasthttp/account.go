package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"hlc/internal/model"
	"hlc/internal/store"
	"net/http"
	"regexp"
	"strconv"
	"time"

	"github.com/valyala/fasthttp"
)

var (
	availableStatus = map[string]bool{
		"свободны":   true,
		"заняты":     true,
		"всё сложно": true,
	}
	availableGender = map[string]bool{
		"m": true,
		"f": true,
	}
	emailRegExp = regexp.MustCompile(`[a-zA-Z0-9-.]+@[a-z0-9.]+\.[a-z]+`)
)

func createAccount(ctx *fasthttp.RequestCtx) {

	start := time.Now()

	var input model.Account

	body := ctx.Request.Body()

	if err := json.NewDecoder(bytes.NewReader(body)).Decode(&input); err != nil {
		noContentHandler(ctx, http.StatusBadRequest)
		return
	}

	if input.ID == 0 {
		noContentHandler(ctx, http.StatusBadRequest)
		return
	}

	if _, ok := availableStatus[input.Status]; !ok {
		noContentHandler(ctx, http.StatusBadRequest)
		return
	}

	if _, ok := availableGender[input.Gender]; !ok {
		noContentHandler(ctx, http.StatusBadRequest)
		return
	}

	defer func() {
		if since := time.Since(start); since > time.Millisecond*500 {
			fmt.Printf("Insert %.5f\n", since.Seconds())
		}
	}()

	if err := store.Create(input); err != nil {
		noContentHandler(ctx, http.StatusBadRequest)
		return
	}

	noContentHandler(ctx, http.StatusCreated)
}

func updateAccount(uid string) fasthttp.RequestHandler {

	return func(ctx *fasthttp.RequestCtx) {

		start := time.Now()

		id, err := strconv.Atoi(uid)
		if err != nil {
			noContentHandler(ctx, http.StatusNotFound)
			return
		}

		var input model.UpdAcc

		body := ctx.Request.Body()

		if err := json.NewDecoder(bytes.NewReader(body)).Decode(&input); err != nil {
			noContentHandler(ctx, http.StatusBadRequest)
			return
		}

		if g := input.Gender; g != "" {
			if g != "m" && g != "f" {
				noContentHandler(ctx, http.StatusBadRequest)
				return
			}
		}

		if input.Status != "" {
			if _, ok := availableStatus[input.Status]; !ok {
				noContentHandler(ctx, http.StatusBadRequest)
				return
			}
		}

		if input.Email != "" {
			if ok := emailRegExp.MatchString(input.Email); !ok {
				noContentHandler(ctx, http.StatusBadRequest)
				return
			}
		}

		defer func() {
			if since := time.Since(start); since > time.Millisecond*500 {
				fmt.Printf("Update %.5f\n", since.Seconds())
			}
		}()

		if err := store.Update(int32(id), input); err != nil {
			if err == store.ErrUserNotFound {
				noContentHandler(ctx, http.StatusNotFound)
			} else {
				noContentHandler(ctx, http.StatusBadRequest)
			}
			return
		}

		noContentHandler(ctx, http.StatusAccepted)
	}
}
