package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"hlc/internal/model"
	"hlc/internal/store"
	"net/http"
	"time"

	"github.com/valyala/fasthttp"
)

func createLikes(ctx *fasthttp.RequestCtx) {

	start := time.Now()
	var likes model.LikeRequest

	body := ctx.Request.Body()

	if err := json.NewDecoder(bytes.NewReader(body)).Decode(&likes); err != nil {
		noContentHandler(ctx, http.StatusBadRequest)
		return
	}

	if len(likes.List) == 0 {
		noContentHandler(ctx, http.StatusAccepted)
		return
	}

	defer func() {
		if since := time.Since(start); since > time.Millisecond*500 {
			fmt.Printf("Insert %.5f\n", since.Seconds())
		}
	}()

	if err := store.InsertLikes(likes); err != nil {
		noContentHandler(ctx, http.StatusBadRequest)
		return
	}

	noContentHandler(ctx, http.StatusAccepted)
}
